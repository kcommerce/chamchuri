#!/bin/bash

path=$1
file=$2

#OUT_DIR="/home/eanmand/eniq/data/pmdata/eniq_oss_1/smsc/FcatData/"
OUT_DIR="/tmp/"


cd $path


sitename=`echo $file |cut -d"_" -f1`
filetype=`echo $file |cut -d"_" -f2`
filedate=`echo $file |cut -d"_" -f3`
filetime=`echo $file |cut -d"_" -f4`


#****************HourlySMSDeliverAttempt********************
if [ "$filetype" = "HourlySMSDeliverAttempt" ]
then
echo "SITE_NAME,SDR_RECORD_TYPE,REPORT_TYPE,NODE_NAME,ORIG_NETWORK_TYPE,DEST_NET_TYPE,RECIP_APP_ID,DEST_SMS_TYPE,MSG_TYPE,MSG_ERROR_CODE,MSG_LAST_DELIVERY_TIME,Submit_Count"



#****************HourlySMSDeliverCharging********************
elif [ "$filetype" = "HourlySMSDeliverCharging" ]
then
echo "SITE_NAME,SDR_RECORD_TYPE,REPORT_TYPE,NODE_NAME,RDBL_FLAG_HEX,MSG_TYPE,MT_IMSI,SDR_LOG_TIME,Submit_Count"



#****************HourlySMSDeliverSuccess********************
elif [ "$filetype" = "HourlySMSDeliverSuccess" ]
then
echo "SITE_NAME,SDR_RECORD_TYPE,REPORT_TYPE,NODE_NAME,ORIG_NETWORK_TYPE,DEST_NET_TYPE,RECIP_APP_ID,DEST_SMS_TYPE,MSG_TYPE,MSG_ERROR_CODE,MSG_LAST_DELIVERY_TIME,SM_NUM_OF_ATTEMPTS,LATENCY,Submit_Count"



#****************HourlySMSSubmit********************
elif [ "$filetype" = "HourlySMSSubmit" ]
then
echo "SITE_NAME,SDR_RECORD_TYPE,REPORT_TYPE,NODE_NAME,ORIG_NETWORK_TYPE,ORIG_APP_ID,ORIG_SMS_TYPE,MSG_ERROR_CODE,MSG_ORIG_SUBM_TIME,Submit_Count"



#****************HourlySpamDetect********************
elif [ "$filetype" = "HourlySpamDetect" ]
then
echo "SITE_NAME,REPORT_TYPE,Spam_Detected,SCCP_ORIG_ADDR_GT_ADDR,ORIG_ADDRESS_ADDRESS,MO_IMSI,MT_SC_ADDR_ADDRESS,REJECT_REASON,MSG_ORIG_SUBM_TIME,Submit_Count"



#****************MPSSMSDeliverAttempt********************
elif [ "$filetype" = "MPSSMSDeliverAttempt" ]
then
echo "SITE_NAME,SDR_RECORD_TYPE,REPORT_TYPE,NODE_NAME,ORIG_NETWORK_TYPE,DEST_NET_TYPE,RECIP_APP_ID,DEST_SMS_TYPE,MSG_TYPE,MSG_ERROR_CODE,MSG_LAST_DELIVERY_TIME,Submit_MPS"



#****************MPSSMSDeliverSuccess********************
elif [ "$filetype" = "MPSSMSDeliverSuccess" ]
then
echo "SITE_NAME,SDR_RECORD_TYPE,REPORT_TYPE,NODE_NAME,ORIG_NETWORK_TYPE,DEST_NET_TYPE,RECIP_APP_ID,DEST_SMS_TYPE,MSG_TYPE,MSG_ERROR_CODE,MSG_LAST_DELIVERY_TIME,SM_NUM_OF_ATTEMPTS,LATENCY,Submit_MPS"



#****************MPSSMSSubmit********************
elif [ "$filetype" = "MPSSMSSubmit" ]
then
echo "SITE_NAME,SDR_RECORD_TYPE,REPORT_TYPE,NODE_NAME,ORIG_NETWORK_TYPE,ORIG_APP_ID,ORIG_SMS_TYPE,MSG_ERROR_CODE,MSG_ORIG_SUBM_TIME,Submit_MPS"

fi

cat $file
