#!/bin/bash
#
# Copyright (c) 2013 Ericsson (Thailand) Ltd.
# All rights reserved.
#
# The information in this document is the property of Ericsson.
# Except as specifically authorized in writing by Ericsson, the
# receiver of this document shall keep the information contained
# herein confidential and shall protect the same in whole or in
# part from disclosure and dissemination to third parties.
# Disclosure and dissemination to the receiver's employees shall
# only be made on a strict need to know basis.
#
#
# Application : GYC
#
# --------------------------------------------------------------------------------
# Revision History
# --------------------------------------------------------------------------------
# Rev      Date         Name          Description
# -----    ----------   -----------   --------------------------------------------
# 1.0      03/05/2012   ECT/EATTPUM   Created
# 1.5		15/05/2013  PRTR/Sakchart Update Gyc module
# 2.0	   23/07/2013	PRTR/Sakchart Updated with sendAlarm

name_translate () {

        LC_PATTERN=$1
        LC_STAT_FILE=$2
        LC_STAT_NODE_PREFIX=$3
        LC_STAT_NODE_IPADDR=$4

        case "${LC_PATTERN}" in

        'performance_*')
        LC_STAT_FILE=${LC_STAT_FILE//.dat}
        LC_STAT_FILE="${LC_STAT_NODE_PREFIX}${LC_STAT_FILE}-${LC_STAT_NODE_IPADDR}.dat"
        ;;

        'counters_*')
        LC_STAT_FILE=${LC_STAT_FILE//.dat}
        LC_STAT_FILE="${LC_STAT_NODE_PREFIX}${LC_STAT_FILE}-${LC_STAT_NODE_IPADDR}.dat"
        ;;

        *)
        ;;
        esac
}

LC_ARGS_NUMBERS=9


if [ $# -lt ${LC_ARGS_NUMBERS} ]; then
        echo "Usage : $0 NE_NAME NE_IP_ADDRESS NE_USERNAME NE_REMOTE_PRE_DIR NE_REMOTE_POST_DIR NE_STAT_PREFIX NE_STAT_LIST NE_LOCAL_PRE_DIR NE_LOCAL_POST_DIR"
        exit 1
fi

#LC_LIB_NAME=collect_handler_01
LC_LIB_NAME=`basename $0`
LC_STAT_FILE=""

LC_NE_NAME=$1
LC_NE_ADDR=$2
LC_NE_USER=$3
LC_NE_REM_PRE=$4
LC_NE_REM_POS=$5
LC_NE_PREFIX=$6
LC_NE_STAT_LIST=$7
LC_NE_LOC_PRE=$8
LC_NE_LOC_POS=$9

LC_NE_TYPE=${10}

LC_WORKING="WORKING"
LC_ARCHIVE="ARCHIVE"
LC_MAX_TRANSFER=30

# script to send alarm
# sendAlarm.sh [HOSTNAME] [ALARM DESCRIPTION] [ALARM NUMBER]
# send Alarm
ALARM_APP="/eniq/backup/ctp_collector/bin/sendAlarm.sh"
ALARM_HOST="deqerst1"
ALARM_NO=30312
ALARM1_NO=`expr $ALARM_NO + 0`
ALARM2_NO=`expr $ALARM_NO + 1`
ALARM3_NO=`expr $ALARM_NO + 2`
ALARM4_NO=`expr $ALARM_NO + 3`
ALARM5_NO=`expr $ALARM_NO + 4`
ALARM6_NO=`expr $ALARM_NO + 5`



#echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME}"

# check whether host is available
ssh ${LC_NE_USER}@${LC_NE_ADDR} 'hostname'

if [ $? -gt 0 ];then
	echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] HOST:${LC_NE_ADDR} is not available; SSH Exit code=$?"
	$ALARM_APP $ALARM_HOST "$ALARM1 : ${LC_NE_ADDR}" $ALARM1_NO
    exit 1
fi

#ssh ${LC_NE_USER}@${LC_NE_ADDR} "cd ${LC_NE_REM_PRE}; ls -l ${LC_NE_PREFIX}" | grep ^- | awk '{print $9}' > ${LC_NE_STAT_LIST} 2>&1
ssh ${LC_NE_USER}@${LC_NE_ADDR} "cd ${LC_NE_REM_PRE}; ls -l ${LC_NE_PREFIX}" | grep ^- | awk '{print $9} END { if (NR>=1) {exit 0;} else exit 7;}' > ${LC_NE_STAT_LIST} 2>&1

if [ $? == "0" ]; then
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME} success"
else
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [WARN] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME} failed"
        exit 0
fi

if [ -s ${LC_NE_STAT_LIST} ]; then

        #for TRANSFER_LIST in `head -${LC_MAX_TRANSFER} ${LC_NE_STAT_LIST}`
        for TRANSFER_LIST in `cat ${LC_NE_STAT_LIST}`
        do

                LC_STAT_FILE=${TRANSFER_LIST}

                case "${LC_NE_TYPE}" in

                'gyc_stat') name_translate ${LC_NE_PREFIX} ${TRANSFER_LIST} "" ${LC_NE_ADDR}

                        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Transfering stat ${PREFIX_LIST}:${LC_NE_NAME}:${TRANSFER_LIST}->${LC_STAT_FILE}"

                        scp -q ${LC_NE_USER}@${LC_NE_ADDR}:${LC_NE_REM_PRE}/${TRANSFER_LIST} ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} > /dev/null 2>&1

                        if [ $? == 0 ]; then
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-transfer ${TRANSFER_LIST} to local PRE directory SUCCESS"
                        else
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] Pre-transfer ${TRANSFER_LIST} to local PRE directory FAILED"
                                
                                $ALARM_APP $ALARM_HOST "$ALARM2 : ${LC_NE_ADDR}" $ALARM2_NO
                        fi

                        ssh ${LC_NE_USER}@${LC_NE_ADDR} "mv ${LC_NE_REM_PRE}/${TRANSFER_LIST} ${LC_NE_REM_POS}/${TRANSFER_LIST}" > /dev/null 2>&1

                        if [ $? == 0 ]; then
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-transfer for ${TRANSFER_LIST} SUCCESS"
                        else
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] Post-transfer for ${TRANSFER_LIST} FAILED"
                                $ALARM_APP $ALARM_HOST "$ALARM3 : ${LC_NE_ADDR}:${LC_NE_REM_POS}/${TRANSFER_LIST}" $ALARM3_NO
                        fi

                        cp ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} ${LC_NE_LOC_POS}/${LC_STAT_FILE}

                        if [ $? == 0 ]; then
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-distributor ${LC_STAT_FILE} to POST direcotry SUCCESS"
                                chmod 664 ${LC_NE_LOC_POS}/${LC_STAT_FILE}
                        else
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-distributor ${LC_STAT_FILE} to POST direcotry FAILED"
                                $ALARM_APP $ALARM_HOST "$ALARM4 : ${LC_NE_LOC_POS}/${LC_STAT_FILE}" $ALARM4_NO
                        fi

                        mv ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}

                        if [ $? == 0 ]; then
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-distributor ${LC_STAT_FILE} to ARCHIVE direcotry SUCCESS"
                                chmod 664  ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}
                        else
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-distributor ${LC_STAT_FILE} to ARCHIVE direcotry FAILED"
                                $ALARM_APP $ALARM_HOST "$ALARM5 : ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}" $ALARM5_NO
                        fi

                        ;;


                *) echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] No handler for ${LC_NE_TYPE}"
                        ;;
                esac
        done
else
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] No stat in queue for ${PREFIX_LIST} from ${LC_NE_NAME}"
        exit 1
fi