#!/bin/bash
#
# Copyright (c) 2013 Ericsson (Thailand) Ltd.
# All rights reserved.
#
# The information in this document is the property of Ericsson.
# Except as specifically authorized in writing by Ericsson, the
# receiver of this document shall keep the information contained
# herein confidential and shall protect the same in whole or in
# part from disclosure and dissemination to third parties.
# Disclosure and dissemination to the receiver's employees shall
# only be made on a strict need to know basis.
#
#
# Application : SMS
#
# --------------------------------------------------------------------------------
# Revision History
# --------------------------------------------------------------------------------
# Rev      Date         Name          Description
# -----    ----------   -----------   --------------------------------------------
# 1.0      03/05/2012   ECT/EATTPUM   Created
# 2.0	   23/07/2013	PRTR/Sakchart Updated with sendAlarm

name_translate () {

        LC_PATTERN=$1
        LC_STAT_FILE=$2
        LC_STAT_NODE_PREFIX=$3
        LC_STAT_NODE_IPADDR=$4

        case "${LC_PATTERN}" in

        'performance_*')
        LC_STAT_FILE=${LC_STAT_FILE//.dat}
        LC_STAT_FILE="${LC_STAT_NODE_PREFIX}-${LC_STAT_FILE}-${LC_STAT_NODE_IPADDR}.dat"
        ;;

        *)
        ;;
        esac
}

LC_ARGS_NUMBERS=9


if [ $# -lt ${LC_ARGS_NUMBERS} ]; then
        echo "Usage : $0 NE_NAME NE_IP_ADDRESS NE_USERNAME NE_REMOTE_PRE_DIR NE_REMOTE_POST_DIR NE_STAT_PREFIX NE_STAT_LIST NE_LOCAL_PRE_DIR NE_LOCAL_POST_DIR"
        exit 1
fi

#LC_LIB_NAME=collect_handler_01
LC_LIB_NAME=`basename $0`


LC_STAT_FILE=""

LC_NE_NAME=$1
LC_NE_ADDR=$2
LC_NE_USER=$3
LC_NE_REM_PRE=$4
LC_NE_REM_POS=$5
LC_NE_PREFIX=$6
LC_NE_STAT_LIST=$7
LC_NE_LOC_PRE=$8
LC_NE_LOC_POS=$9

LC_NE_TYPE=${10}

LIB_ADD_HEADER=/eniq/backup/ctp_collector/lib/smsc-add-header.sh
TMP_DIR=/eniq/backup/ctp_collector/tmp

LC_WORKING="WORKING"
LC_ARCHIVE="ARCHIVE"


# send Alarm
ALARM_APP="/eniq/backup/ctp_collector/bin/sendAlarm.sh"
ALARM_HOST="deqerst1"
ALARM_NO=30318
ALARM1_NO=`expr $ALARM_NO + 0`
ALARM2_NO=`expr $ALARM_NO + 1`
ALARM3_NO=`expr $ALARM_NO + 2`
ALARM4_NO=`expr $ALARM_NO + 3`
ALARM5_NO=`expr $ALARM_NO + 4`
ALARM6_NO=`expr $ALARM_NO + 5`


ALARM1="failed to SSH "
ALARM2="failed to SCP "
ALARM3="failed to move to POST path"
ALARM4="failed to copy to ENIQ path"
ALARM5="failed to move to archive path "
ALARM6="failed to run; process is already running"




#echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Start ssh list ${PREFIX_LIST} on remote ${LC_NE_NAME} ${LC_NE_ADDR}"


# check whether host is available
ssh ${LC_NE_USER}@${LC_NE_ADDR} 'hostname'

if [ $? -gt 0 ];then
	echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] HOST:${LC_NE_ADDR} is not available; SSH Exit code=$?"
	$ALARM_APP $ALARM_HOST "$ALARM1 : ${LC_NE_ADDR}" $ALARM1_NO
    exit 1
fi

ssh ${LC_NE_USER}@${LC_NE_ADDR} "cd ${LC_NE_REM_PRE}; ls -l ${LC_NE_PREFIX}" | grep ^- | awk '{print $9}' > ${LC_NE_STAT_LIST} 2>&1

if [ $? == "0" ]; then
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME} success"
else
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [WARN] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME} failed"
        exit 0
fi

if [ -s ${LC_NE_STAT_LIST} ]; then
        #head -10 ${LC_NE_STAT_LIST} | while read TRANSFER_LIST

    	TOTAL_FILE=`cat ${LC_NE_STAT_LIST} | wc -l| sed 's/ //g'`
        NO_FILE=0
        
        for TRANSFER_LIST in `cat ${LC_NE_STAT_LIST}`
        do
        	   # init index file
               NO_FILE=`expr $NO_FILE + 1`
               # assign first without translate or mapping
               LC_STAT_FILE=${TRANSFER_LIST}

               echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Transfering stat [$NO_FILE/$TOTAL_FILE] ${PREFIX_LIST}:${LC_NE_NAME}:${TRANSFER_LIST}->${LC_STAT_FILE}"

        case "${LC_NE_TYPE}" in


        'smsc_stat')

		s1="scp-1"
        scp -q ${LC_NE_USER}@${LC_NE_ADDR}:${LC_NE_REM_PRE}/${TRANSFER_LIST} ${TMP_DIR}/${LC_STAT_FILE} > /dev/null 2>&1                                      

		if [ $? == 0 ]; then
		#echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-transfer ${TRANSFER_LIST} to local PRE directory SUCCESS"
		s1="scp_ok"
		else
		s1="scp_error"
		
		$ALARM_APP $ALARM_HOST "$ALARM2 : ${LC_NE_ADDR}" $ALARM2_NO
		
		fi
                                			
        # step 2: add header and move file to post dir on remote node
        s2a="addheader-1"
        s2="ssh_move-1"
    
                        


            # just warn if file size is zero
	        if [ ! -s ${TMP_DIR}/${LC_STAT_FILE} ]
	        then
        	echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [WARN] ${TMP_DIR}/${LC_STAT_FILE} size is zero"
        	fi

           # add header and write output to PRE directory
        	${LIB_ADD_HEADER} ${TMP_DIR} ${LC_STAT_FILE} > ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE}
			if [ $? == 0 ]; then
				s2a="addheader-ok"
			else
				s2a="addheader-error"


        	rm ${TMP_DIR}/${LC_STAT_FILE}
                                                   
        ssh ${LC_NE_USER}@${LC_NE_ADDR} "mv ${LC_NE_REM_PRE}/${TRANSFER_LIST} ${LC_NE_REM_POS}/${TRANSFER_LIST}" > /dev/null 2>&1

        if [ $? == 0 ]; then
        # echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-transfer@${LC_NE_ADDR} for ${TRANSFER_LIST} SUCCESS"
		s2="ssh_move_ok"
        else
        #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] Post-transfer@${LC_NE_ADDR} for ${TRANSFER_LIST} FAILED"
		s2="ssh_move_error"
		
		$ALARM_APP $ALARM_HOST "$ALARM3 : ${LC_NE_ADDR}:${LC_NE_REM_POS}/${TRANSFER_LIST}" $ALARM3_NO
        fi

        # step 3: copy file to ENIQ dir
        s3="copy2eniq-1"
                        
        cp ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} ${LC_NE_LOC_POS}/${LC_STAT_FILE}

        if [ $? == 0 ]; then
        #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-distributor ${LC_STAT_FILE} to POST direcotry SUCCESS"
        chmod 664 ${LC_NE_LOC_POS}/${LC_STAT_FILE}
         s3="copy2eniq_ok"
        else
        #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-distributor ${LC_STAT_FILE} to POST direcotry FAILED"
		 s3="copy2eniq_error"
		
		$ALARM_APP $ALARM_HOST "$ALARM4 : ${LC_NE_LOC_POS}/${LC_STAT_FILE}" $ALARM4_NO
        fi

		# step 4: move to archive dir
		s4="move2archive-1"
		
		mv ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}

		if [ $? == 0 ]; then
        #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-distributor ${LC_STAT_FILE} to ARCHIVE direcotry SUCCESS"
        chmod 664 ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}
         s4="move2archive-ok"
        else
        #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-distributor ${LC_STAT_FILE} to ARCHIVE direcotry FAILED"
        s4="move2archive-error"
        
        $ALARM_APP $ALARM_HOST "$ALARM5 : ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}" $ALARM5_NO
		fi

                                                                                       


                        # ****** summary step *******
                        s_result="$s1$s2a$s2$s3$s4"

                        if [ $s_result == "scp_okaddheader-okssh_move_okcopy2eniq_okmove2archive-ok" ]; then
                        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] [${LC_STAT_FILE}] all steps success: $s1:$s2:$s3:$s4 "
                        else
                        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] [${LC_STAT_FILE}] some steps failed: $s1:$s2:$s3:$s4 "

                        fi

                        if [ $NO_FILE == $TOTAL_FILE ]; then

                        echo  "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Finished $TOTAL_FILE files "
                        fi





                         ;;

                        *) echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] No handler for ${LC_NE_TYPE}"
                        ;;
                        esac






        done
else
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [WARN] No files with prefix:${PREFIX_LIST} on remote ${LC_NE_NAME}@${LC_NE_ADDR}"
        exit 1
fi
