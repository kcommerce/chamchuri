#!/bin/bash
#
# Copyright (c) 2013 Ericsson(Thailand)Ltd.
# All rights reserved.
#
# The information in this document is the property of Ericsson.
# Except as specifically authorized in writing by Ericsson, the
# receiver of this document shall keep the information contained
# herein confidential and shall protect the same in whole or in
# part from disclosure and dissemination to third parties.
# Disclosure and dissemination to the receiver's employees shall
# only be made on a strict need to know basis.
#
#
# Application : EMA
#
# --------------------------------------------------------------------------------
# Revision History
# --------------------------------------------------------------------------------
# Rev      Date         Name          Description
# -----    ----------   -----------   --------------------------------------------
# 1.0      9/7/2013    OS/Sakchart    Started; copied from Atthaya
# 2.0	   23/07/2013   PRTR/Sakchart   Updated with sendAlarm

name_translate () {

        LC_PATTERN=$1
        LC_STAT_FILE=$2
        LC_STAT_NODE_PREFIX=$3
        LC_STAT_NODE_IPADDR=$4

        case "${LC_PATTERN}" in

        'counters*')
        LC_STAT_FILE=${LC_STAT_FILE//.dat}
        LC_STAT_FILE="${LC_STAT_NODE_PREFIX}${LC_STAT_FILE}-${LC_STAT_NODE_IPADDR}.dat"
        ;;

        'performance*')
        LC_STAT_FILE=${LC_STAT_FILE//.dat}
        LC_STAT_FILE="${LC_STAT_NODE_PREFIX}${LC_STAT_FILE}-${LC_STAT_NODE_IPADDR}.dat"
        ;;

        *)
        ;;
        esac
}

LC_ARGS_NUMBERS=9


if [ $# -lt ${LC_ARGS_NUMBERS} ]; then
        echo "Usage : $0 NE_NAME NE_IP_ADDRESS NE_USERNAME NE_REMOTE_PRE_DIR NE_REMOTE_POST_DIR NE_STAT_PREFIX NE_STAT_LIST NE_LOCAL_PRE_DIR NE_LOCAL_POST_DIR"
        exit 1
fi

#LC_LIB_NAME=collect_handler_01
LC_LIB_NAME=`basename $0`


LC_STAT_FILE=""

LC_NE_NAME=$1
LC_NE_ADDR=$2
LC_NE_USER=$3
LC_NE_REM_PRE=$4
LC_NE_REM_POS=$5
LC_NE_PREFIX=$6
LC_NE_STAT_LIST=$7
LC_NE_LOC_PRE=$8
LC_NE_LOC_POS=$9

LC_NE_TYPE=${10}

LC_ARCHIVE="ARCHIVE"
LC_WORKING="WORKING"
LC_MAX_TRANSFER=50

# script to send alarm
# sendAlarm.sh [HOSTNAME] [ALARM DESCRIPTION] [ALARM NUMBER]
# send Alarm
ALARM_APP="/eniq/backup/ctp_collector/bin/sendAlarm.sh"
ALARM_HOST="deqerst1"
ALARM_NO=30306
ALARM1_NO=`expr $ALARM_NO + 0`
ALARM2_NO=`expr $ALARM_NO + 1`
ALARM3_NO=`expr $ALARM_NO + 2`
ALARM4_NO=`expr $ALARM_NO + 3`
ALARM5_NO=`expr $ALARM_NO + 4`
ALARM6_NO=`expr $ALARM_NO + 5`


#echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME}"

# check whether host is available
ssh ${LC_NE_USER}@${LC_NE_ADDR} 'hostname'

if [ $? -gt 0 ];then
	echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] HOST:${LC_NE_ADDR} is not available; SSH Exit code=$?"
	$ALARM_APP $ALARM_HOST "$ALARM1 : ${LC_NE_ADDR}" $ALARM1_NO
    exit 1
fi
    	
# NOTE: EMA is based on linux, filename is in the 8th column
#ssh ${LC_NE_USER}@${LC_NE_ADDR} "cd ${LC_NE_REM_PRE}; ls -l ${LC_NE_PREFIX}" | grep ^- | awk '{print $8}' > ${LC_NE_STAT_LIST} 2>&1
ssh ${LC_NE_USER}@${LC_NE_ADDR} "cd ${LC_NE_REM_PRE}; ls -l ${LC_NE_PREFIX}" | grep ^- | awk '{print $8} END { if (NR>=1) {exit 0;} else exit 7;}' > ${LC_NE_STAT_LIST} 2>&1

if [ $? == "0" ]; then
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME} success"
else
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Getting list of ${PREFIX_LIST} from ${LC_NE_NAME} failed"
                
        exit 0
fi

if [ -s ${LC_NE_STAT_LIST} ]; then
        #head -10 ${LC_NE_STAT_LIST} | while read TRANSFER_LIST
        #for TRANSFER_LIST in `head -${LC_MAX_TRANSFER} ${LC_NE_STAT_LIST}`
        TOTAL_FILE=`cat ${LC_NE_STAT_LIST} | wc -l| sed 's/ //g'`
        NO_FILE=0
        for TRANSFER_LIST in `cat ${LC_NE_STAT_LIST}`
        do
               # init index file
               NO_FILE=`expr $NO_FILE + 1`
               # assign first without translate or mapping
               LC_STAT_FILE=${TRANSFER_LIST}

               echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] [$NO_FILE/$TOTAL_FILE] Transfering stat ${PREFIX_LIST}:${LC_NE_NAME}:${LC_NE_ADDR}:${TRANSFER_LIST}"

               case "${LC_NE_TYPE}" in

               'ema_stat') name_translate ${LC_NE_PREFIX} ${TRANSFER_LIST} "" ${LC_NE_ADDR}
               #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] MAPPING ${TRANSFER_LIST}->${LC_STAT_FILE}"
                        # step 1: scp from origin node
                        s1="scp-1"
                        scp -pq ${LC_NE_USER}@${LC_NE_ADDR}:${LC_NE_REM_PRE}/${TRANSFER_LIST} ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} > /dev/null 2>&1

                        if [ $? == 0 ]; then
                                #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-transfer ${TRANSFER_LIST} to local PRE directory SUCCESS"
                                s1="scp_ok"
                        else
                                #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] Pre-transfer ${TRANSFER_LIST} to local PRE directory FAILED"
                                s1="scp_error"
                                
                                $ALARM_APP $ALARM_HOST "$ALARM2 : ${LC_NE_ADDR}" $ALARM2_NO
                        fi

                        # step 2: move file to post dir on origin node
                        s2="ssh_move-1"

                        ssh ${LC_NE_USER}@${LC_NE_ADDR} "mv ${LC_NE_REM_PRE}/${TRANSFER_LIST} ${LC_NE_REM_POS}/${TRANSFER_LIST}" > /dev/null 2>&1

                        if [ $? == 0 ]; then
                                #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-transfer for ${TRANSFER_LIST} SUCCESS"
                                s2="ssh_move_ok"
                        else
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] Post-transfer for ${TRANSFER_LIST} FAILED"
                                s2="ssh_move_error"
                                $ALARM_APP $ALARM_HOST "$ALARM3 : ${LC_NE_ADDR}:${LC_NE_REM_POS}/${TRANSFER_LIST}" $ALARM3_NO
                        fi

                        # step 3: copy file to ENIQ dir
                        s3="copy2eniq-1"

                        cp ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} ${LC_NE_LOC_POS}/${LC_STAT_FILE}

                        if [ $? == 0 ]; then
                                #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-distributor ${LC_STAT_FILE} to POST direcotry SUCCESS"
                                chmod 664 ${LC_NE_LOC_POS}/${LC_STAT_FILE}
                                s3="copy2eniq_ok"
                        else
                                #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Pre-distributor ${LC_STAT_FILE} to POST direcotry FAILED"
                                s3="copy2eniq_error"
                                $ALARM_APP $ALARM_HOST "$ALARM4 : ${LC_NE_LOC_POS}/${LC_STAT_FILE}" $ALARM4_NO
                        fi

                        # step 4: move to archive dir
                        s4="move2archive-1"

                        mv ${LC_NE_LOC_PRE}/${LC_WORKING}/${LC_STAT_FILE} ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}

                        if [ $? == 0 ]; then
                                #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-distributor ${LC_STAT_FILE} to ARCHIVE direcotry SUCCESS"
                                chmod 664  ${LC_NE_LOC_PRE}/${LC_ARCHIVE}/${LC_STAT_FILE}
                                s4="move2archive-ok"
                        else
                                #echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Post-distributor ${LC_STAT_FILE} to ARCHIVE direcotry FAILED"
                                s4="move2archive-error"
                        fi

                        # ****** summary step *******
                        s_result="$s1$s2$s3$s4"

                        if [ $s_result == "scp_okssh_move_okcopy2eniq_okmove2archive-ok" ]; then
                        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] [${LC_STAT_FILE}] all steps success: $s1:$s2:$s3:$s4 "
                        else
                        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] [${LC_STAT_FILE}] some steps failed: $s1:$s2:$s3:$s4 "

                        fi

                        if [ $NO_FILE == $TOTAL_FILE ]; then

                        echo  "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] Finished $TOTAL_FILE files "
                        fi

                        ;;

               *) echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [ERROR] No handler for ${LC_NE_TYPE}"
                        ;;
               esac
        done
else
        echo "`date "+%Y%m%d_%H:%M:%S"` ${LC_LIB_NAME} [INFO] No stat in queue for ${PREFIX_LIST} from ${LC_NE_NAME}"
        exit 1
fi
