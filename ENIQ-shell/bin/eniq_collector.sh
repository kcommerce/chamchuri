#!/bin/bash
#
# Copyright (c) 2013 Ericsson(Thailand) Ltd.
# All rights reserved.
#
# The information in this document is the property of Ericsson.
# Except as specifically authorized in writing by Ericsson, the
# receiver of this document shall keep the information contained
# herein confidential and shall protect the same in whole or in
# part from disclosure and dissemination to third parties.
# Disclosure and dissemination to the receiver's employees shall
# only be made on a strict need to know basis.
#
#
# Application : eniq_collector_smsc.sh
#
# --------------------------------------------------------------------------------
# Revision History
# --------------------------------------------------------------------------------
# Rev      Date         Name          Description
# -----    ----------   -----------   --------------------------------------------
# 1.0      03/05/2013   ECT/EATTPUM   Created
# 1.1      14/05/2013   OS/Sakchart   Updated smsc module
# 1.2      15/05/2013   ECT/EATTPUM   Updated gyc module, multi profile
# 1.3      15/05/2013   OS/Sakchart   Updated directory path to ctpuser path,gyc module
# 1.4      17/05/2013   OS/Sakchart   Fixed bug to have own C_TEMP_FILE and C_LOAD_FILE and checkwhether file exist before running
# 1.5      20/05/2013   OS/Sakchart   Add "umask 002" to allow group to write
# 1.6      24/06/2013   OS/Sakchart   move C_HOME_DIR

cfg_parser ()
{

        echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Profile loading..."
        egrep "^[A-Z]|^[a-z]|^\[" $1 > ${C_TEMP_FILE}

        ini="$(< ${C_TEMP_FILE})"                       # read the file
        ini="${ini//[/\[}"                              # escape [
        ini="${ini//]/\]}"                              # escape ]
        IFS=$'\n' && ini=( ${ini} )                     # convert to line-array
        ini=( ${ini[*]//;*/} )                          # remove comments with ;
        ini=( ${ini[*]/\    =/=} )                      # remove tabs before =
        ini=( ${ini[*]/=\   /=} )                       # remove tabs be =
        ini=( ${ini[*]/\ =\ /=} )                       # remove anything with a space around =
        ini=( ${ini[*]/#\\[/\}$'\n'cfg.section.} )      # set section prefix
        ini=( ${ini[*]/%\\]/ \(} )                      # convert text2function (1)
        ini=( ${ini[*]/=/=\( } )                        # convert item to array
        ini=( ${ini[*]/%/ \)} )                         # close array parenthesis
        ini=( ${ini[*]/%\\ \)/ \\} )                    # the multiline trick
        ini=( ${ini[*]/%\( \)/\(\) \{} )                # convert text2function (2)
        ini=( ${ini[*]/%\} \)/\}} )                     # remove extra parenthesis
        ini[0]=""                                       # remove first element
        ini[${#ini[*]} + 1]='}'                         # add the last brace
        eval "$(echo "${ini[*]}")"                      # eval the result

}

if [ $# -ne 1 ]; then
        echo "Usage: $0 DEVEICE_ABBREAVATION (DDC|GYC|SMSC|EMA)"
        exit 1
fi

# set default umask
# allow group to write access

umask 002

C_ABBREVATION=$1
C_APPLICATION=eniq_collector
C_HOME_DIR=/eniq/backup/ctp_collector
C_PROFILE_CONF=${C_HOME_DIR}/etc/profile.conf

C_TEMP_FILE=${C_HOME_DIR}/tmp/profile.tmp.$1
C_LOAD_FILE=${C_HOME_DIR}/tmp/profile.load.$1

C_LIB_DDC_GET_STAT=${C_HOME_DIR}/lib/collector-getstat-ddc.sh
C_LIB_GYC_GET_STAT=${C_HOME_DIR}/lib/collector-getstat-gyc.sh
C_LIB_SMS_GET_STAT=${C_HOME_DIR}/lib/collector-getstat-smsc.sh
C_LIB_EMA_GET_STAT=${C_HOME_DIR}/lib/collector-getstat-ema.sh

# check whether there running old process via C_TEMP_FILE
if [  -f ${C_TEMP_FILE} ]; then
        echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [WARN] ${C_ABBREVATION} is still running"
exit 1
fi

# Check device abbrevation name support list
################################################################

grep ${C_ABBREVATION} ${C_HOME_DIR}/etc/profile.conf

if [ $? != 0 ]; then
        echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [ERROR] Parameter device name not in list"
        exit 1
fi

# Check devices config profiles
################################################################

if [ ! -f ${C_PROFILE_CONF} ]; then
        echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [ERROR] Profile configuration was not found!"
        exit 0
else
        echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Extracting config from ${C_PROFILE_CONF}"
fi


grep ^${C_ABBREVATION} ${C_PROFILE_CONF} | while read PROFILES_LIST
do
        LC_NODE=`echo ${PROFILES_LIST} | cut -d, -f1`
        LC_PATH=`echo ${PROFILES_LIST} | cut -d, -f2`
        LC_FILE=`echo ${PROFILES_LIST} | cut -d, -f3`

        echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Extracting profile of ${LC_NODE} from ${LC_PATH}/${LC_FILE}"

        if [ ! -f ${LC_PATH}/${LC_FILE} ]; then
                echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [ERROR] Profile ${LC_NODE} :: ${LC_PATH}/${LC_FILE} not found"
        else

                # Call module cfg_parser to load profile into environment variable
                cfg_parser ${LC_PATH}/${LC_FILE}

                IFS=' '$'\n'
                fun="$(declare -F)"
                fun="${fun//declare -f/}"
                for f in $fun; do
                        [ "${f#cfg.section}" == "${f}" ] && continue
                        item="$(declare -f ${f})"
                        echo "${item}" > ${C_TEMP_FILE}
                        sed -e 's/ //g' -e 's/;//g' -e 's/(//g' -e 's/)//g' -e 's/\|/ /g' ${C_TEMP_FILE} | grep ^NE > ${C_LOAD_FILE}
                        source ${C_LOAD_FILE}
                        cat ${C_LOAD_FILE}

                        echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Stat handler selecting"

                        case "${NE_TYPE}" in

                        'ddc_stat') echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Stat handler selected->${NE_TYPE}"
                                for STAT_PREFIX in ${NE_REM_STAT_PREFIX}
                                do
                                        ${C_LIB_DDC_GET_STAT} ${NE_NAME} ${NE_ADDRESS} ${NE_ACCOUNT} ${NE_REM_STAT_PRE} ${NE_REM_STAT_POST} ${STAT_PREFIX} ${NE_LOC_STAT_LIST} ${NE_LOC_STAT_PRE} ${NE_LOC_STAT_POST} ${NE_TYPE}
                                done
                                ;;

                        'gyc_stat') echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Stat handler selected->${NE_TYPE}"
                                for STAT_PREFIX in ${NE_REM_STAT_PREFIX}
                                do
                                        ${C_LIB_GYC_GET_STAT} ${NE_NAME} ${NE_ADDRESS} ${NE_ACCOUNT} ${NE_REM_STAT_PRE} ${NE_REM_STAT_POST} ${STAT_PREFIX} ${NE_LOC_STAT_LIST} ${NE_LOC_STAT_PRE} ${NE_LOC_STAT_POST} ${NE_TYPE}
                                done
                                ;;

                        'smsc_stat') echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Stat handler selected->${NE_TYPE}."
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] PREFIX:[${NE_REM_STAT_PREFIX}]"
                                for STAT_PREFIX in ${NE_REM_STAT_PREFIX}
                                do
                                        ${C_LIB_SMS_GET_STAT} ${NE_NAME} ${NE_ADDRESS} ${NE_ACCOUNT} ${NE_REM_STAT_PRE} ${NE_REM_STAT_POST} ${STAT_PREFIX} ${NE_LOC_STAT_LIST} ${NE_LOC_STAT_PRE} ${NE_LOC_STAT_POST} ${NE_TYPE}
                                done
                                ;;
                        'ema_stat') echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] Stat handler
 selected->${NE_TYPE}."
                                echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [INFO] PREFIX:[${NE_REM_STAT_PREFIX}]"
                                for STAT_PREFIX in ${NE_REM_STAT_PREFIX}
                                do
                                        ${C_LIB_EMA_GET_STAT} ${NE_NAME} ${NE_ADDRESS} ${NE_ACCOUNT} ${NE_REM_STAT_PRE} ${NE_REM_STAT_POST} ${STAT_PREFIX} ${NE_LOC_STAT_LIST} ${NE_LOC_STAT_PRE} ${NE_LOC_STAT_POST} ${NE_TYPE}
                                done
                                ;;

                        *) echo "`date "+%Y%m%d_%H:%M:%S"` ${C_APPLICATION} [ERROR] No stat process handler support"
                                ;;
                        esac
                done
        fi
done

# cleaning up
rm ${C_TEMP_FILE}
rm ${C_LOAD_FILE}


exit 0
