#!/bin/bash
#
# Copyright (c) 2007-2009 Ericsson AB.
# All rights reserved.
#
# The information in this document is the property of Ericsson.
# Except as specifically authorized in writing by Ericsson, the
# receiver of this document shall keep the information contained
# herein confidential and shall protect the same in whole or in
# part from disclosure and dissemination to third parties.
# Disclosure and dissemination to the receiver's employees shall
# only be made on a strict need to know basis.
#
#
# -----------------------------------------------------------------
# Revision History
# -----------------------------------------------------------------
# Rev      Date         Name        Description
# -----    ----------   --------    -------------------------------
# PA1      04/07/2013   EATTPUM     Created
#
#

CBS_BASE=/eniq/backup/ctp_collector
CBS_NODE=`hostname`
CBS_CONF=${CBS_BASE}/etc/retention.conf
CBS_LOCK_FILE=${CBS_BASE}/tmp/CBS-retention.lock

#CBS_BACKUP_LOG="${CBS_BASE}/log/cbsRetension.`date "+%Y%m%d_%H%M"`.${CBS_NODE}.log"
CBS_BACKUP_LOG="${CBS_BASE}/log/cbsRetension.log"
CBS_BACKUP_S_MSG="`date "+%Y%m%d_%H:%M:%S"` ENIQ_RETENSION [INFO] Node is primary application starting"
CBS_BACKUP_E_MSG="`date "+%Y%m%d_%H:%M:%S"` ENIQ_RETENSION [INFO]  Node not in primary state application will not start"

# check whether lock file still exists
if [  -f ${CBS_LOCK_FILE} ]; then
        echo " ENIQ_RETENSION [ERROR] application is still running; check ${CBS_LOCK_FILE}"
        echo "`date "+%Y%m%d_%H:%M:%S"` ENIQ_RETENSION [ERROR] application is still running;check ${CBS_LOCK_FILE}"  >> ${CBS_BACKUP_LOG}
exit 1
fi

# touch lock file
touch ${CBS_LOCK_FILE}

if [ ! -f ${CBS_CONF} ]; then
        echo "`date "+%Y%m%d %H:%M:%S"` ENIQ_RETENSION [ERROR] Configuration file not found, terminate" >> ${CBS_BACKUP_LOG}
        exit 1
fi

source ${CBS_CONF}

grep ^[A-Z] ${CBS_NE_LIST} | while read NE_LIST
do
        LC_NW=`echo ${NE_LIST} | cut -d\, -f1`
        LC_NE=`echo ${NE_LIST} | cut -d\, -f2`
        LC_CONF=`echo ${NE_LIST} | cut -d\, -f3`

        echo "`date "+%Y%m%d %H:%M:%S"` ENIQ_RETENSION [INFO] Extract NE list for ${LC_NW}/${LC_NE} from ${LC_CONF}" >> ${CBS_BACKUP_LOG}

        LC_PROFILE="${CBS_NE_PATH}/${LC_CONF}"

        if [ ! -f ${LC_PROFILE} ]; then
                echo "`date "+%Y%m%d %H:%M:%S"` ENIQ_RETENSION [ERROR] NE profile not found, terminate" >> ${CBS_BACKUP_LOG}
                exit 1
        fi

        grep ^[A-Z] ${LC_PROFILE} | while read BK_LIST
        do
                LC_BK_NW=`echo ${BK_LIST} | cut -d\, -f1`
                LC_BK_NE=`echo ${BK_LIST} | cut -d\, -f2`
                LC_BK_TYPE=`echo ${BK_LIST} | cut -d\, -f3`
                LC_BK_PATH=`echo ${BK_LIST} | cut -d\, -f4`
                LC_BK_MODE=`echo ${BK_LIST} | cut -d\, -f5`
                LC_BK_PREF=`echo ${BK_LIST} | cut -d\, -f6`
                LC_BK_SUFF=`echo ${BK_LIST} | cut -d\, -f7`
                LC_BK_DAYS=`echo ${BK_LIST} | cut -d\, -f8`
                echo "CONFIG: ${BK_LIST} "  >> ${CBS_BACKUP_LOG}

                echo "`date "+%Y%m%d %H:%M:%S"` ENIQ_RETENSION [INFO] List of backup for ${LC_BK_NW}/${LC_BK_NE}/${LC_BK_TYPE} from ${LC_BK_PATH}" >> ${CBS_BACKUP_LOG}

                if [ ${LC_BK_MODE} != "F" ]; then

                        echo "=====================================================================" >> ${CBS_BACKUP_LOG}
                        find ${LC_BK_PATH} -mtime +${LC_BK_DAYS} | while read DEL_LIST
                        do
                                echo -e "\tENIQ_RETENSION [INFO] Deleting => ${DEL_LIST}" >> ${CBS_BACKUP_LOG}
                        done
                        LC_RM_DEL=`find ${LC_BK_PATH} -mtime +${LC_BK_DAYS} | wc -l | sed -e "s/ //g"`
                        echo -e "\n\tENIQ_RETENSION [INFO] Total delete = ${LC_RM_DEL}" >> ${CBS_BACKUP_LOG}

                        ##################################################################################
                        # CAUTION!! -- when enable next command below retension action delete will engage
                        #
                        find ${LC_BK_PATH} -mtime +${LC_BK_DAYS} -exec rm -rf {} \;
                        #
                        ##################################################################################

                else
                        echo "==================================================================" >> ${CBS_BACKUP_LOG}

                        find ${LC_BK_PATH} -name "${LC_BK_PREF}*${LC_BK_SUFF}" -mtime +${LC_BK_DAYS} | while read DEL_LIST
                        do
                                echo -e "\tENIQ_RETENSION [INFO] Deleting => ${DEL_LIST}" >> ${CBS_BACKUP_LOG}
                        done
                        LC_RM_DEL=`find ${LC_BK_PATH} -name "${LC_BK_PREF}*${LC_BK_SUFF}" -mtime +${LC_BK_DAYS} | wc -l | sed -e "s/ //g"`
                        echo -e "\n\tENIQ_RETENSION [INFO] Total delete = ${LC_RM_DEL}" >> ${CBS_BACKUP_LOG}

                        ##################################################################################
                        # CAUTION!! -- when enable next command below retension action delete will engage
                        #
                        find ${LC_BK_PATH} -name "${LC_BK_PREF}*${LC_BK_SUFF}" -mtime +${LC_BK_DAYS} -exec rm -f {} \;
                        #
                        ##################################################################################

                fi
        done
done

# delete lock file to allow next run
rm ${CBS_LOCK_FILE}
