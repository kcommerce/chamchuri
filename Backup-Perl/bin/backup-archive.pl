#!/usr/bin/perl
# ************************************************************
# Program: backup_main.pl
# Author: Sakchart Ngamluan
#                 sakchart.ngamluan@bangkoksiservice.com
#
# Date: 5 June 2013
# Description: backup script for EGW & Archive CrimeLaw
#
# Copyright (c) 2013 Ericsson(Thailand) Ltd.
# Disclaimer: Ericsson has no responsibility from modifying or using
#                         this script without Ericsson's supervision
# ************************************************************

use POSIX qw( strftime );
my $today = strftime("%Y-%m-%d", localtime);

use Sys::Hostname;
my $HOSTNAME = hostname;



#******************************************************************************
# ***** check if lock file exist ****
$APP_PATH="/var/opt/mediation/ora/ECTbackup";
$BIN_PATH="$APP_PATH/bin";
$LOG_PATH="$APP_PATH/logs";

$LOCK_FILE="$BIN_PATH/$HOSTNAME.backup.lock";
$TAR_PATH="$APP_PATH/BACKUP";

$TAR_FILE="$HOSTNAME-backup-$today.tar";
$LOG_FILE="$LOG_PATH/$HOSTNAME-log-$today.log";
$CONFIG_FILE="$APP_PATH/conf/$HOSTNAME.conf";
$CONFIG_FILE2="$APP_PATH/conf/global.conf";
$CRON_FILE="$APP_PATH/WORKING/$HOSTNAME-crontab";

$ORACLE_IP="XXXX"; # disable global.conf

$INTERFACE=`/usr/sbin/ifconfig -a`;

$LOG_DEL_AGE = 14;
$log_file_pattern = "\"$HOSTNAME-\*.log\"";
$backup_file_pattern= "\"$HOSTNAME-\*.gz\"";

$EMC_PATH="/var/opt/archive2/archive";

#$SCP_OPTION="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -r";
#$SCP_USER_HOST="criuser\@10.26.76.141";
#$SCP_REMOTE_PATH="/var/opt/archive2/crime";

######### Alarm to OSS Details

$ALARM_APP="/var/opt/mediation/ora/ECTbackup/bin/sendAlarm.sh";



#******************************************************************************
sub log {
($str) = @_;

$date =`date +'%Y-%m-%d'` ;
$time=`date +'%H:%M:%S'` ;
chomp($time);
chomp($date);
open(RAWLOG,">> $LOG_PATH/$HOSTNAME-backup-$date.log");
print RAWLOG "$time $str \n";
close(RAWLOG);

}
sub logInfo {
        ($str) = @_;
        print ("[INFO] " . $str ."\n");
        &log("[INFO ]" . $str);
}

sub logError {
        ($str) = @_;
        print ("[ERROR] " . $str . "\n");
        &log("[ERROR] " . $str);
}

if ( -e $LOCK_FILE )
{
        &logInfo("backup process is still running.");
        &logInfo("run ps -ef and check file: $LOCK_FILE.");

        $ALARM_ID = substr($HOSTNAME,length($HOSTNAME)-1,1);
        $ALARM_ID = 30147 + $ALARM_ID;
        $alarm_command = "$ALARM_APP $HOSTNAME \'$HOSTNAME failed to run; process is already running\' $ALARM_ID";
        system("$alarm_command");
        &logInfo("Send Alarm: $alarm_command");


        exit 1;

} else {
# Mark lock file to start working

        system("touch $LOCK_FILE");
}

unless ( -e $CONFIG_FILE )
{
        &logError( "configuration does not exist: $CONFIG_FILE..");
        exit 1;
}
#unless ( -e $CONFIG_FILE2 )
#{
#        &logError("configuration does not exist: $CONFIG_FILE2..");
#        exit 1;
#}


# ******** dump crontab ******************

system("crontab -l root > $CRON_FILE.root");
#system("crontab -l oracle > $CRON_FILE.oracle");

# ********Read config and start backup**********

open(FILE,"$CONFIG_FILE");
@config=<FILE>;
close(FILE);

@files;


foreach $line(@config)
{
        chomp($line);
        if (length($line)>=1 && index($line,"\#") < 0 )
        {
        #print "[$line]\n";
        push(@files,$line);
        }
}

$no_file = @files;
if ($no_file > 0 )
{
        &logInfo("Start tar files= $no_file");

        $file = shift(@files);
        #print "** $file\n";

        system("tar cvf $TAR_PATH/$TAR_FILE $file");

        if ( -e "$TAR_PATH/$TAR_FILE" )
        {
                        &logInfo(" Tar File: $TAR_PATH/$TAR_FILE created.");
        foreach $file (@files)
        {
                #print ">>$file\n";
                system("tar rvf $TAR_PATH/$TAR_FILE $file");


        }
                } else {

                        &logError(" Expect File: $TAR_PATH/$TAR_FILE does not exist");
                }
}

# ************* perform global backup if oracle is here************

#if (index($INTERFACE,$ORACLE_IP) >= 0 && -e "$TAR_PATH/$TAR_FILE" )
#{
#
#&logInfo("VIP $ORACLE_IP is running; perform global backup");
#
#open(FILE,"$CONFIG_FILE2");
#@config=<FILE>;
#close(FILE);
#
#
#foreach $line(@config)
#{
#        chomp($line);
#        if (length($line)>=1 && index($line,"\#") < 0 )
#        {
#        #print "[$line]\n";
#        push(@files,$line);
#        }
#}
#
#$no_file = @files;
#if ($no_file > 0 )
#{
#        if ( -e "$TAR_PATH/$TAR_FILE" )
#        {
#        foreach $file (@files)
#        {
#                #print ">>$file\n";
#                system("tar rvf $TAR_PATH/$TAR_FILE $file");
#        }
#
#        }
#}
#
#
#
#}

# *********************

if ( -e  "$TAR_PATH/$TAR_FILE" )
{
                &logInfo("Start gzip: $TAR_PATH/$TAR_FILE");
        system("gzip -f $TAR_PATH/$TAR_FILE");
                if ($?== 0)
                {
                        &logInfo("Gzip Success.");
                } else {
                        &logError("Gzip Error!!");
                }
}

# ********************************************************************************
# **** Transfer file to Archive server
# ********************************************************************************
#
#$out=system("scp $SCP_OPTION $TAR_PATH/$TAR_FILE\.gz $SCP_USER_HOST:$SCP_REMOTE_PATH");
#&logInfo("$out");
#
#if ($?== 0)
#{
#       &logInfo("scp $TAR_PATH/$TAR_FILE\.gz success");
#
#       #${ALARM_LIB} ${ALARM_SITE} ${ALARM_SYSTEM} ${ALARM_OSS_NE} "Backup Transfer FAILED for ${CBS_TRANSFER_TYPE}" | ssh ${ALARM_USER}@${ALARM_HOST} 'cat >> ${ALARM_FILE}'
#       $alarm_command = "$ALARM_LIB $ALARM_SITE $ALARM_SYSTEM $ALARM_OSS_NE \"Backup Transfer FAILED for \" \| ssh $ALARM_USER\@$ALARM_HOST 'cat \>\> $ALARM_FILE'";
#       &logInfo("Alarm: $alarm_command");
#
#} else {
#
#       &logError("scp $TAR_PATH/$TAR_FILE\.gz failed");
#}
#

# ***** backup file to EMC *****
  if ( -e "$TAR_PATH/$TAR_FILE\.gz" )
  {
      system("cp $TAR_PATH/$TAR_FILE\.gz $EMC_PATH");
          if ($?== 0)
          {
                &logInfo("copy $TAR_PATH/$TAR_FILE\.gz to $EMC_PATH Success");

          } else {
                &logError("copy $TAR_PATH/$TAR_FILE\.gz to $EMC_PATH Failed");
        $ALARM_ID = substr($HOSTNAME,length($HOSTNAME)-1,1);
        $ALARM_ID = 30145 + $ALARM_ID;
        $alarm_command = "$ALARM_APP $HOSTNAME \'$HOSTNAME failed to copy to EMC path\' $ALARM_ID";
        system("$alarm_command");
        &logInfo("Send Alarm: $alarm_command");

          }
  } else {

        &logError("File: $TAR_PATH/$TAR_FILE\.gz does not exist");
  }


# **** delete backup gz file ****

      if ( -e "$TAR_PATH/$TAR_FILE\.gz" )
          {

            system("rm -rf $TAR_PATH/$TAR_FILE\.gz");
                if ($?== 0)
                {
                        &logInfo("Delete $TAR_PATH/$TAR_FILE\.gz success.");
                } else {
                        &logError("Delete $TAR_PATH/$TAR_FILE\.gz failed.");
                }

          } else {
                &logError("File $TAR_PATH/$TAR_FILE does\.gz not exist.");
          }

          # *** To add logic to delete files older than LOG_DEL_AGE days ****



$list_command ="find $LOG_PATH -type f -name ". $log_file_pattern. " -mtime +". $LOG_DEL_AGE ." -exec ls -lrt {} \\;";

$del_command = "find $LOG_PATH -type f -name ". $log_file_pattern. " -mtime +". $LOG_DEL_AGE ." -exec rm {} \\;";

$del_command2 = "find $EMC_PATH -type f -name ". $backup_file_pattern. " -mtime +". $LOG_DEL_AGE ." -exec rm {} \\;";

&logInfo("Run: $list_command");
$out = `$list_command`;
&logInfo($out);

&logInfo("Run: $del_command");
$out = `$del_command`;
&logInfo($out);

&logInfo("Run: $del_command2");
$out = `$del_command2`;
&logInfo($out);


# ***** delete lock file *******
system("rm $LOCK_FILE");

