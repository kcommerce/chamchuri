#!/bin/bash
#0 2 * * * /var/opt/mediation/ora/ECTbackup/bin/egwBackup.sh

cd /var/opt/mediation/ora/ECTbackup/bin
HOSTNAME=`hostname`
/var/opt/mediation/ora/ECTbackup/bin/backup.pl > /var/opt/mediation/ora/ECTbackup/logs/$HOSTNAME-cron.log  2>&1
