#!/bin/bash
#0 2 * * * /var/opt/mediation/ora/ECTbackup/bin/archiveBackup.sh

cd /var/opt/mediation/ora/ECTbackup/bin
HOSTNAME=`hostname`
/var/opt/mediation/ora/ECTbackup/bin/backup-archive.pl > /var/opt/mediation/ora/ECTbackup/logs/$HOSTNAME-cron.log  2>&1
