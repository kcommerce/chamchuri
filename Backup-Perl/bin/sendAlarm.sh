#!/bin/bash

ALARM_TIMESTAMP=`date "+%Y%m%d%H%M%S"`
ALARM_FILE=/tmp/alarm.log
OSS_USER=eric
OSS_ADDR=10.26.77.4
#OSS_TXF_FILE=/etc/opt/ericsson/fm/txf/Process/txf_ENIQ_adapt_1/Interface/AlarmReceptionFile1
OSS_TXF_FILE=/etc/opt/ericsson/fm/txf/Process/txf_OSMC_adapt_1/Interface/AlarmReceptionFile1
OSS_FQDN="SubNetwork=ONRM_ROOT_MO,SubNetwork=BACKUP,ManagedElement=DARCRST1-PS"

cat > ${ALARM_FILE} <<EOF
%a
-ObjectOfReference=${OSS_FQDN}
-EventTime=${ALARM_TIMESTAMP}
-EventType=888
-RecordType=2
-PerceivedSeverity=4
-AlarmNumber=$3
-SpecificProblemText=$2
-ProblemText=ObjectElement:Ericsson/RST/$1, Timestamp:${ALARM_TIMESTAMP:0:8}_${ALARM_TIMESTAMP:8:6}, Descripton:SAPCMessage
%A
EOF

if [ -f ${ALARM_FILE} ]; then
        cat ${ALARM_FILE} | ssh ${OSS_USER}@${OSS_ADDR} "cat >> ${OSS_TXF_FILE}"
fi
