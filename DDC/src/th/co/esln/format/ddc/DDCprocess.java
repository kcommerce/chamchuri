/*      */ package th.co.esln.format.ddc;
/*      */ 
/*      */ import java.io.BufferedReader;
/*      */ import java.io.BufferedWriter;
/*      */ import java.io.DataInputStream;
/*      */ import java.io.File;
/*      */ import java.io.FileInputStream;
/*      */ import java.io.FileWriter;
/*      */ import java.io.InputStreamReader;
/*      */ import java.text.DateFormat;
/*      */ import java.text.SimpleDateFormat;
/*      */ import java.util.ArrayList;
/*      */ import java.util.Calendar;
/*      */ import java.util.Date;
/*      */ import java.util.Iterator;
/*      */ import java.util.Locale;
/*      */ import th.co.esln.util.TLogger;
/*      */ 
/*      */ public class DDCprocess
/*      */ {
/*      */   public static final int NONE = 0;
/*      */   public static final int DES = 1;
/*      */   public static final int MYSQL = 2;
/*      */   public static final int PNP = 3;
/*      */   public static final int SMAF = 4;
/*      */   public static final int TRIGGER = 5;
/*   36 */   public static ArrayList src = new ArrayList();
/*   37 */   public static int mode = 0;
/*      */ 
/*      */   public static long getNext(long curdate, int interval)
/*      */   {
/*   41 */     Date tm = new Date(curdate);
/*      */ 
/*   44 */     Calendar cal = Calendar.getInstance(Locale.US);
/*      */ 
/*   47 */     cal.setTimeInMillis(curdate);
/*      */ 
/*   49 */     int minute = cal.get(12);
/*      */ 
/*   51 */     int output = (minute / interval + 1) * interval;
/*      */ 
/*   55 */     Date tmout = new Date();
/*   56 */     if (output > 59)
/*      */     {
/*   58 */       cal.add(11, 1);
/*   59 */       output = 0;
/*      */     }
/*      */ 
/*   62 */     cal.set(12, output);
/*   63 */     cal.set(13, 0);
/*   64 */     cal.set(14, 0);
/*      */ 
				TLogger.info("[ECT] getNext cur,interval,next:{"+curdate+","+interval+","+cal.getTimeInMillis()+"}");

/*   69 */     return cal.getTimeInMillis();
/*      */   }
/*      */ 
/*      */   public static long getPrevious(long curdate, int interval)
/*      */   {
/*   74 */     Date tm = new Date(curdate);
/*      */ 
/*   77 */     Calendar cal = Calendar.getInstance(Locale.US);
/*      */ 
/*   80 */     cal.setTimeInMillis(curdate);
/*      */ 
/*   82 */     int minute = cal.get(12);
/*      */ 
/*   84 */     int output = minute / interval * interval;
/*      */ 
/*   88 */     Date tmout = new Date();
/*   89 */     if (output > 59)
/*      */     {
/*   91 */       cal.add(11, 1);
/*   92 */       output = 0;
/*      */     }
/*      */ 
/*   95 */     cal.set(12, output);
/*   96 */     cal.set(13, 0);
/*   97 */     cal.set(14, 0);
/*      */ 
/*  102 */     return cal.getTimeInMillis();
/*      */   }
/*      */ 
/*      */   public static triggerInfo2 getGroup2(String token, String errorcode, ArrayList srcx, long stime)
/*      */   {
/*  107 */     triggerInfo2 grp = null;
/*      */ 
/*  109 */     for (Iterator iter = srcx.iterator(); iter.hasNext(); )
/*      */     {
/*  111 */       triggerInfo2 grpx = (triggerInfo2)iter.next();
/*      */ 
/*  113 */       if ((grpx.trig_type.equals(token)) && (grpx.error_code.equals(errorcode)) && (grpx.starttime == stime))
/*      */       {
/*  115 */         grp = grpx; break;
/*      */       }
/*      */     }
/*  118 */     if (grp == null)
/*      */     {
/*  121 */       grp = new triggerInfo2();
/*  122 */       grp.trig_type = token;
/*  123 */       grp.error_code = errorcode;
/*  124 */       grp.starttime = stime;
/*  125 */       srcx.add(grp);
/*      */     }
/*      */ 
/*  128 */     return grp;
/*      */   }
/*      */ 
/*      */   public static triggerInfo getGroup(String token, ArrayList srcx, boolean smaf, long stime)
/*      */   {
/*  134 */     triggerInfo grp = null;
/*      */ 
/*  136 */     for (Iterator iter = srcx.iterator(); iter.hasNext(); )
/*      */     {
/*  138 */       triggerInfo grpx = (triggerInfo)iter.next();
/*      */ 
/*  140 */       if ((grpx.trig_type.equals(token)) && (grpx.smaf == smaf) && (grpx.starttime == stime))
/*      */       {
/*  142 */         grp = grpx; break;
/*      */       }
/*      */     }
/*  145 */     if (grp == null)
/*      */     {
/*  148 */       grp = new triggerInfo();
/*  149 */       grp.trig_type = token;
/*  150 */       grp.smaf = smaf;
/*  151 */       grp.starttime = stime;
/*  152 */       srcx.add(grp);
/*      */     }
/*      */ 
/*  155 */     return grp;
/*      */   }
/*      */ 
/*      */   public static String[] getSplit(String data, String sp, String[] cm)
/*      */   {
/*  161 */     ArrayList x = new ArrayList();
/*      */ 
/*  163 */     boolean ckcm = false;
/*  164 */     String temp = "";
/*  165 */     data = data + sp;
/*      */ 
/*  167 */     for (int i = 0; i < data.length(); ++i)
/*      */     {
/*  169 */       boolean ckx = false;
/*  170 */       for (int j = 0; j < cm.length; ++j)
/*      */       {
/*  172 */         if (!(data.substring(i, i + 1).equals(cm[j])))
/*      */           continue;
/*  174 */         ckx = true;
/*  175 */         break;
/*      */       }
/*      */ 
/*  179 */       if ((data.substring(i, i + 1).equals(sp)) && (!(ckcm)))
/*      */       {
/*  181 */         x.add(temp);
/*  182 */         temp = "";
/*      */       }
/*  184 */       else if (ckx)
/*      */       {
/*  186 */         if (ckcm)
/*  187 */           ckcm = false;
/*      */         else
/*  189 */           ckcm = true;
/*      */       }
/*      */       else
/*      */       {
/*  193 */         temp = temp + data.substring(i, i + 1);
/*      */       }
/*      */ 
/*      */     }
/*      */ 
/*  199 */     String[] output = new String[x.size()];
/*  200 */     int count = 0;
/*  201 */     for (Iterator iter = x.iterator(); iter.hasNext(); )
/*      */     {
/*  203 */       String xx = (String)iter.next();
/*  204 */       output[count] = xx;
/*  205 */       ++count;
/*      */     }
/*  207 */     return output;
/*      */   }
/*      */ 
/*      */   public static void flush(String output, String node, String site)
/*      */   {
/*  212 */     String stime = "";
/*  213 */     if (!(output.endsWith(File.separator))) {
/*  214 */       output = output + File.separator;
/*      */     }
/*  216 */     if (src.size() <= 0)
/*      */       return;
/*      */     Iterator iter;
				//ECT
/*      */    // triggerInfo grp;
/*      */     DateFormat formatter;
/*      */     BufferedWriter out;
/*      */     String outs;
/*      */     Date outtime;
				// ECT
/*      */     //DateFormat formatter;

/*  218 */     if (mode == 1)
/*      */     {
/*  220 */       for (iter = src.iterator(); iter.hasNext(); )
/*      */       {
/*  223 */         triggerInfo grp = (triggerInfo)iter.next();
/*      */         try
/*      */         {
/*  226 */           formatter = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/*  227 */           stime = formatter.format(new Date(grp.filetime));
/*      */         }
/*      */         catch (Exception e)
/*      */         {
/*      */         }
/*      */ 
/*      */         try
/*      */         {
/*  238 */          //ECT 
					//fostream = new FileWriter(output + "DDC_OPER_IN." + stime + ".dat", true);

					 FileWriter fostream = new FileWriter(output + "DDC_OPER_IN." + stime + ".dat", true);
/*  239 */           out = new BufferedWriter(fostream);
/*  240 */           FileWriter fostream2 = new FileWriter(output + "DDC_OPER_PNP." + stime + ".dat", true);
/*  241 */           BufferedWriter out2 = new BufferedWriter(fostream2);
/*  242 */           TLogger.info("Flush output to file(" + output + "DDC_OPER_IN." + stime + ".dat)");
/*  243 */           TLogger.info("Flush output to file(" + output + "DDC_OPER_PNP." + stime + ".dat)");
					//ECT
/*      */          // String outs;
/*      */           //Date outtime;

/*      */           //DateFormat formatter;
/*  244 */           if (grp.smaf)
/*      */           {
/*  246 */             outs = "";
/*  247 */             outs = outs + node + ",";
/*  248 */             outs = outs + site + ",";
/*  249 */             outs = outs + grp.trig_type + ",";
/*      */ 
/*  252 */             outtime = new Date(grp.starttime);
/*      */             try
/*      */             {
/*  255 */               formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  256 */               outs = outs + formatter.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
/*      */             }
/*      */ 
/*  263 */             outs = outs + "S,";
/*  264 */             outs = outs + ",";
/*  265 */             outs = outs + String.valueOf(grp.tran_count) + ",";
/*  266 */             outs = outs + grp.filename;
/*  267 */             out.write(outs + "\n");
/*      */           }
/*      */           else
/*      */           {
/*  271 */             outs = "";
/*  272 */             outs = outs + node + ",";
/*  273 */             outs = outs + site + ",";
/*  274 */             outs = outs + grp.trig_type + ",";
/*      */ 
/*  277 */             outtime = new Date(grp.starttime);
/*      */             try
/*      */             {
/*  280 */               SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  281 */               outs = outs + e.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
/*      */             }
/*      */ 
/*  288 */             outs = outs + "S,";
/*  289 */             outs = outs + ",";
/*  290 */             outs = outs + String.valueOf(grp.tran_count) + ",";
/*  291 */             outs = outs + grp.filename;
/*      */ 
/*  293 */             out2.write(outs + "\n");
/*      */           }
/*  295 */           out.close();
/*  296 */           out2.close();
/*      */         }
/*      */         catch (Exception fostream)
/*      */         {
/*  300 */           TLogger.error("Error: " + fostream.getMessage());
/*      */         }
/*      */ 
/*      */       }
/*      */ 
/*      */     }
/*  306 */     else if (mode != 2)
/*      */     {
				 // ECT
/*      */       //triggerInfo2 grp;
/*  310 */       if (mode == 3)
/*      */       {
/*  312 */         for (iter = src.iterator(); iter.hasNext(); )
/*      */         {
/*  315 */           triggerInfo2 grp2 = (triggerInfo2)iter.next();
/*      */           try
/*      */           {
/*      */             try
/*      */             {
/*  320 */               SimpleDateFormat fostream = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/*  321 */               stime = fostream.format(new Date(grp2.filetime));
/*      */             }
/*      */             catch (Exception fostream)
/*      */             {
/*      */             }
/*      */ 
/*  328 */             FileWriter fostream = new FileWriter(output + "DDC_OPER_PNP." + stime + ".dat", true);
/*  329 */             TLogger.info("Flush output to file(" + output + "DDC_OPER_PNP." + stime + ".dat)");
/*  330 */             out = new BufferedWriter(fostream);
/*      */ 
/*  333 */             outs = "";
/*  334 */             outs = outs + node + ",";
/*  335 */             outs = outs + site + ",";
/*  336 */             outs = outs + grp2.trig_type + ",";
/*      */ 
/*  338 */             outtime = new Date(grp2.starttime);
/*      */             try
/*      */             {
/*  341 */                formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  342 */               outs = outs + formatter.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
/*      */             }
/*      */ 
/*  349 */             outs = outs + "F,";
/*  350 */             outs = outs + grp2.error_code + ",";
/*  351 */             outs = outs + String.valueOf(grp2.tran_count) + ",";
/*  352 */             outs = outs + grp2.filename;
/*  353 */             out.write(outs + "\n");
/*      */ 
/*  355 */             out.close();
/*      */           }
/*      */           catch (Exception fostream)
/*      */           {
/*  359 */             TLogger.error("Error: " + fostream.getMessage());
/*      */           }
/*      */ 
/*      */         }
/*      */ 
/*      */       }
/*  365 */       else if (mode == 4)
/*      */       {
/*  367 */         for (iter = src.iterator(); iter.hasNext(); )
/*      */         {
/*  369 */           triggerInfo2 grp3 = (triggerInfo2)iter.next();
/*      */           try
/*      */           {
					// ECT
/*  372 */             SimpleDateFormat fostream = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/*  373 */             stime = fostream.format(new Date(grp3.filetime));
/*      */           }
/*      */           catch (Exception fostream)
/*      */           {
/*      */           }
/*      */ 
/*      */           try
/*      */           {
						// ECT
/*  383 */             FileWriter fostream = new FileWriter(output + "DDC_OPER_IN." + stime + ".dat", true);
/*  384 */             out = new BufferedWriter(fostream);
/*  385 */             TLogger.info("Flush output to file(" + output + "DDC_OPER_IN." + stime + ".dat)");
/*      */ 
/*  388 */             outs = "";
/*  389 */             outs = outs + node + ",";
/*  390 */             outs = outs + site + ",";
/*  391 */             outs = outs + grp3.trig_type + ",";
/*      */ 
/*  393 */             outtime = new Date(grp3.starttime);
/*      */             try
/*      */             {
						// ECT
/*  396 */               SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  397 */               outs = outs + e.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
/*      */             }
/*      */ 
/*  404 */             outs = outs + "F,";
					
/*  405 */             outs = outs + grp3.error_code + ",";
/*  406 */             outs = outs + String.valueOf(grp3.tran_count) + ",";
/*  407 */             outs = outs + grp3.filename;
/*  408 */             out.write(outs + "\n");
/*      */ 
/*  410 */             out.close();
/*      */           }
/*      */           catch (Exception fostream)
/*      */           {
/*  414 */             TLogger.error("Error: " + fostream.getMessage());
/*      */           }
/*      */ 
/*      */         }
/*      */ 
/*      */       }
/*  421 */       else if (mode == 5)
/*      */       {
/*  423 */         for (iter = src.iterator(); iter.hasNext(); )
/*      */         {
/*  425 */           triggerInfo grp4 = (triggerInfo)iter.next();
/*      */           try
/*      */           {
					// ECT
/*  428 */            SimpleDateFormat  fostream = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/*  429 */             stime = fostream.format(new Date(grp4.filetime));
/*      */           }
/*      */           catch (Exception fostream)
/*      */           {
/*      */           }
/*      */ 
/*      */           try
/*      */           {
						// ECT
/*  438 */             FileWriter fostream = new FileWriter(output + "DDC_OPER_HLR." + stime + ".dat", true);
/*  439 */             TLogger.info("Flush output to file(" + output + "DDC_OPER_HLR." + stime + ".dat)");
/*  440 */             out = new BufferedWriter(fostream);
/*      */ 
/*  442 */             outs = "";
/*  443 */             outs = outs + node + ",";
/*  444 */             outs = outs + site + ",";
/*  445 */             outs = outs + grp4.trig_type + ",";
/*      */ 
/*  447 */             outtime = new Date(grp4.filetime);
/*      */             try
/*      */             {
						//ECT
/*  450 */               SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  451 */               outs = outs + e.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
	//		ECT
							TLogger.error("Error: " + e.getMessage());
/*      */             }
/*      */ 
/*  458 */             outs = outs + String.valueOf(grp4.tran_count) + ",";
/*  459 */             outs = outs + grp4.filename;
/*  460 */             out.write(outs + "\n");
/*  461 */             out.close();
/*      */           }
/*      */           catch (Exception fostream)
/*      */           {
						// ECT
						TLogger.error("Error: " + fostream.getMessage());
/*  465 */            // TLogger.error("Error: " + e.getMessage());
/*      */           }
/*      */         }
/*      */       }
/*      */     }
/*  470 */     src.clear();
/*      */   }
/*      */ 
/*      */   public static boolean grep(String input, String[] condition)
/*      */   {
/*  476 */     int count = 0;
/*  477 */     for (int i = 0; i < condition.length; ++i)
/*      */     {
/*  479 */       int index = condition[i].indexOf("%");
/*  480 */       if (index > -1)
/*      */       {
/*  482 */         String[] term = condition[i].split("%");
/*  483 */         int index1 = input.indexOf(term[0]);
/*  484 */         if (index1 > -1)
/*      */         {
/*  486 */           String input1 = input.substring(index1);
/*  487 */           if (input1.indexOf(",") > -1)
/*      */           {
/*  489 */             input1 = input1.substring(0, input1.indexOf(","));
/*      */           }
/*  491 */           int index2 = input1.indexOf(term[1]);
/*  492 */           if (index2 > -1)
/*      */           {
/*  494 */             ++count;
/*      */           }
/*      */         }
/*      */       }
/*      */       else
/*      */       {
/*  500 */         if (input.indexOf(condition[i]) <= -1)
/*      */           continue;
/*  502 */         ++count;
/*      */       }
/*      */     }
/*      */ 
/*  506 */     return (count == condition.length);
/*      */   }
/*      */ 
/*      */   public static boolean processCounterTrigger(String path, String output, Date dateFromFile, String node, String site, int interval, String stype)
/*      */   {
/*  511 */     mode = 5;
/*  512 */     if (!(output.endsWith(File.separator))) {
/*  513 */       output = output + File.separator;
/*      */     }
/*      */ 
/*      */     try
/*      */     {
/*  525 */       String filepath = path;
/*  526 */       if (new File(filepath).exists())
/*      */       {
/*  528 */         String[] stype_list = null;
/*  529 */         if (stype.indexOf(",") > -1)
/*      */         {
/*  531 */           stype_list = stype.split(",");
/*  532 */           for (int y = 0; y < stype_list.length; ++y)
/*      */           {
/*  534 */             stype_list[y] = "stype=" + stype_list[y] + ",";
/*      */           }
/*      */ 
/*      */         }
/*  538 */         else if (stype.length() > 0)
/*      */         {
/*  540 */           stype_list = new String[1];
/*  541 */           stype_list[0] = stype;
/*      */         }
/*      */ 
/*  545 */         String stime = "";
/*      */         try
/*      */         {
/*  548 */           DateFormat formatter = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/*  549 */           stime = formatter.format(dateFromFile);
/*      */         }
/*      */         catch (Exception e)
/*      */         {
/*      */         }
/*      */ 
/*  557 */         int sep = filepath.lastIndexOf(File.separator);
/*  558 */         int dot = filepath.lastIndexOf(".");
/*  559 */         TLogger.info("start process DDC Trigger counter file (" + filepath + ") output(" + output + "DDC_OPER_HLR." + stime + ".dat" + ")");
/*      */ 
/*  566 */         FileInputStream fstream = new FileInputStream(filepath);
/*      */ 
/*  568 */         DataInputStream in = new DataInputStream(fstream);
/*  569 */         BufferedReader br = new BufferedReader(new InputStreamReader(in));
/*  570 */         FileWriter fostream = new FileWriter(output + "DDC_OPER_HLR." + stime + ".dat", true);
/*  571 */         BufferedWriter out = new BufferedWriter(fostream);
/*      */ 
/*  576 */         src.clear();
/*  577 */         String[] cm = { "{", "}", "[", "]" };
/*  578 */         String[] con1 = { "trigger_protocol=udcd", "stype=6,", "event_reason=New_IMSI" };
/*  579 */         String[] con2 = { "trigger_protocol=udcd", "stype=111,", "event_reason=New_IMSI" };
/*  580 */         String[] con3 = { "trigger_protocol=udcd", "event_reason=New_IMSI" };
/*  581 */         String[] con4 = { "trigger_protocol=udcd", "event_reason=Removal_of_User" };
/*  582 */         String[] con5 = { "trigger_protocol=udcd", "event_reason=IMSI_Changeover" };
/*  583 */         String[] con6 = { "trigger_protocol=udcd", "event_reason=MSISDN_Change" };
/*  584 */         String[] con7 = { "trigger_protocol=udcd", "event_reason=Device_Change" };
/*  585 */         String[] con8 = { "trigger_protocol=udcd", "event_reason=%_update" };
/*  586 */         long lasttime = 0L;
/*      */ 
					//ECT
					String strLine;
					
				 
					 
					 
/*  588 */         while ((strLine = br.readLine()) != null)
/*      */         {
/*      */       
					TLogger.info("[ECT] Dump line:{"+strLine+"}");
					
/*  592 */           String[] token = getSplit(strLine, " ", cm);
/*  593 */           String trig_type = "";

					//----------------------------------------- Begin change ---------------------------------------------

					TLogger.info("[ECT] Dump token[6] :{"+token[6]+"}");
/*  594 */           if (grep(token[6], con1))
/*      */           {
/*  596 */             trig_type = "prepaid numbering pool";
/*      */           }
/*  598 */           else if (grep(token[6], con2))
/*      */           {
/*  600 */             trig_type = "postpaid ready SIM";
/*      */           }
/*  602 */           else if (grep(token[6], con3))
/*      */           {
/*  604 */             if (stype_list != null)
/*      */             {
/*  606 */               for (int y = 0; y < stype_list.length; ++y)
/*      */               {
/*  608 */                 if (token[6].indexOf(stype_list[y]) <= -1)
/*      */                   continue;
/*  610 */                 trig_type = "prepaid fix number";
/*      */               }
/*      */ 
/*      */             }
/*      */ 
/*  616 */             if (trig_type.equals(""))
/*  617 */               trig_type = "postpaid normal";
/*      */           }
/*  619 */           else if (grep(token[6], con4))
/*      */           {
/*  621 */             trig_type = "delete subscriber";
/*      */           }
/*  623 */           else if (grep(token[6], con5))
/*      */           {
/*  625 */             trig_type = "imsi changeover";
/*      */           }
/*  627 */           else if (grep(token[6], con6))
/*      */           {
/*  629 */             trig_type = "msisdn changeover";
/*      */           }
/*  631 */           else if (grep(token[6], con7))
/*      */           {
/*  633 */             trig_type = "imei change";
/*      */           }
/*  635 */           else if (grep(token[6], con8))
/*      */           {
/*  637 */             trig_type = "update transaction";
/*      */           }

					TLogger.info("[ECT] Trig_type:{"+trig_type+"}");
/*      */ 
		//----------------------------------------- End change ---------------------------------------------

/*  642 */           String timestamp = token[0] + " " + token[1].split(",")[0];
/*  643 */           long dateInLong = 0L;
/*      */           try
/*      */           {
/*  646 */             DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  647 */             Date date = formatter.parse(timestamp);
/*  648 */             dateInLong = date.getTime();
/*      */           }
/*      */           catch (Exception e)
/*      */           {
						TLogger.error(e.getMessage());
						e.printStackTrace();
/*      */           }


					 TLogger.info("[ECT] Last time:{"+lasttime+"}");
					 TLogger.info("[ECT] dateInLong:{"+lasttime+"}");
					 TLogger.info("[ECT] interval:{"+interval+"}");
/*      */ 
/*  655 */           if ((lasttime > 0L) && (dateInLong >= getNext(lasttime, interval)))
/*      */           {
/*  657 */             TLogger.info("Write output to file(" + output + "DDC_OPER_HLR." + stime + ".dat)");
/*  658 */             for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */             {
/*  660 */               triggerInfo grp = (triggerInfo)iter.next();
/*      */ 
/*  662 */               String outs = "";
/*  663 */               outs = outs + node + ",";
/*  664 */               outs = outs + site + ",";
/*  665 */               outs = outs + grp.trig_type + ",";
/*      */ 
/*  667 */               Date outtime = new Date(getPrevious(lasttime, interval));
/*      */               try
/*      */               {
/*  670 */                 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  671 */                 outs = outs + formatter.format(outtime) + ",";
/*      */               }
/*      */               catch (Exception e)
/*      */               {
/*      */               }
/*      */ 
/*  678 */               outs = outs + String.valueOf(grp.tran_count) + ",";
/*  679 */               outs = outs + filepath.substring(sep + 1);
/*  680 */               out.write(outs + "\n");
/*      */             }
/*      */ 
/*  685 */             src.clear();
/*      */           }
/*      */ 
/*  688 */           lasttime = dateInLong;
/*      */ 
/*  690 */           if (!(trig_type.equals("")))
/*      */           {
/*  692 */             triggerInfo grp = getGroup(trig_type, src, false, getNext(lasttime, interval));
/*  693 */             grp.filename = filepath.substring(sep + 1);
/*  694 */             grp.filetime = dateFromFile.getTime();
/*  695 */             grp.tran_count += 1;
/*      */           }
/*      */ 
/*      */         }
/*      */ 
/*  701 */         if (src.size() > 0)
/*      */         {
/*  703 */           TLogger.info("Write output to file(" + output + "DDC_OPER_HLR." + stime + ".dat)");
/*  704 */           for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */           {
/*  706 */             triggerInfo grp = (triggerInfo)iter.next();
/*      */ 
/*  708 */             String outs = "";
/*  709 */             outs = outs + node + ",";
/*  710 */             outs = outs + site + ",";
/*  711 */             outs = outs + grp.trig_type + ",";
/*      */ 
/*  713 */             Date outtime = new Date(getPrevious(lasttime, interval));
/*      */             try
/*      */             {
/*  716 */               DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  717 */               outs = outs + formatter.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
/*      */             }
/*      */ 
/*  724 */             outs = outs + String.valueOf(grp.tran_count) + ",";
/*  725 */             outs = outs + filepath.substring(sep + 1);
/*  726 */             out.write(outs + "\n");
/*      */           }
/*      */ 
/*  731 */           src.clear();
/*      */         }
/*      */ 
/*  736 */         out.close();
/*  737 */         in.close();
/*      */ 
/*  740 */         TLogger.info("complete process DDC Trigger counter file(" + output + "DDC_OPER_HLR." + stime + ".dat" + ")");
/*      */ 
/*  742 */         return true;
/*      */       }
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/*  749 */       TLogger.error("Error: " + e.getMessage());
/*  750 */       return false;
/*      */     }
/*      */ 
/*  753 */     return false;
/*      */   }
/*      */ 
/*      */   public static boolean processCounterDes(String path, String output, Date dateFromFile, String node, String site, int interval)
/*      */   {
/*  758 */     mode = 1;
/*  759 */     if (!(output.endsWith(File.separator))) {
/*  760 */       output = output + File.separator;
/*      */     }
/*      */ 
/*      */     try
/*      */     {
/*  772 */       String filepath = path;
/*  773 */       if (new File(filepath).exists())
/*      */       {
/*  776 */         int sep = filepath.lastIndexOf(File.separator);
/*  777 */         int dot = filepath.lastIndexOf(".");
/*  778 */         TLogger.info("start process DDC DES counter file (" + filepath + ")");
/*      */ 
/*  784 */         String stime = "";
/*      */         try
/*      */         {
/*  787 */           DateFormat formatter = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/*  788 */           stime = formatter.format(dateFromFile);
/*      */         }
/*      */         catch (Exception e)
/*      */         {
/*      */         }
/*      */ 
/*  796 */         FileInputStream fstream = new FileInputStream(filepath);
/*      */ 
/*  798 */         DataInputStream in = new DataInputStream(fstream);
/*  799 */         BufferedReader br = new BufferedReader(new InputStreamReader(in));
/*  800 */         FileWriter fostream = new FileWriter(output + "DDC_OPER_IN." + stime + ".dat", true);
/*  801 */         BufferedWriter out = new BufferedWriter(fostream);
/*  802 */         FileWriter fostream2 = new FileWriter(output + "DDC_OPER_PNP." + stime + ".dat", true);
/*  803 */         BufferedWriter out2 = new BufferedWriter(fostream2);
/*      */ 
/*  809 */         src.clear();
/*  810 */         boolean smaf = false;
/*  811 */         String[] cm = { "{", "}", "[", "]" };
/*  812 */         String[] con1 = { "[Interface.triggerNotification]", "SMAFInterface" };
/*  813 */         String[] con2 = { "[Interface.triggerNotification]", "PNPInterface" };
// ECT fix
// original: /*  814 */         String[] con3 = { "74", "75", "76", "77", "78", "79", "80", "81", "82" };
/*  814 */         String[] con3 = { "49","50","51","52","53","54","55","56","57","74", "75", "76", "77", "78", "79", "80", "81", "82" };
/*      */ 
/*  817 */         long lasttime = 0L;
/*      */ 
					// ECT
					String strLine;
/*  819 */         while ((strLine = br.readLine()) != null)
/*      */         {
/*      */          
/*  822 */           String[] token = getSplit(strLine, " ", cm);
/*  823 */           String trig_type = "xxx";
/*      */           String stype;
/*      */           int st;
/*      */           int sp;
/*      */           int k;
/*  824 */           if (grep(strLine, con1))
/*      */           {
/*  826 */             smaf = true;
/*  827 */             stype = "";
/*  828 */             st = strLine.indexOf("stype");
/*      */ 
/*  830 */             if (st > -1)
/*      */             {
/*  832 */               sp = strLine.indexOf(",", st);
/*  833 */               stype = strLine.substring(st + 6, sp);
/*      */             }
/*      */ 
/*  836 */             if (!(stype.equals("")))
/*      */             {
/*  838 */               for (k = 0; k < con3.length; ++k)
/*      */               {
/*  840 */                 if (!(con3[k].equals(stype)))
/*      */                   continue;
/*  842 */                 trig_type = "Prepaid Fix Number";
/*      */ 
/*  844 */                 break;
/*      */               }
/*      */ 
/*      */             }
/*      */ 
/*      */           }
/*  850 */           else if (grep(strLine, con2))
/*      */           {
/*  852 */             smaf = false;
/*  853 */             stype = "";
/*  854 */             st = strLine.indexOf("stype");
/*      */ 
/*  856 */             if (st > -1)
/*      */             {
/*  858 */               k = strLine.indexOf(",", st);
/*  859 */               stype = strLine.substring(st + 6, k);
/*      */             }
/*      */ 
/*  862 */             if (!(stype.equals("")))
/*      */             {
/*  864 */               for (k = 0; k < con3.length; ++k)
/*      */               {
/*  866 */                 if (!(con3[k].equals(stype))) {
/*      */                   continue;
/*      */                 }
/*  869 */                 trig_type = "Prepaid Fix Number";
/*  870 */                 break;
/*      */               }
/*      */ 
/*      */             }
/*      */ 
/*  875 */             if (stype.equals("6"))
/*      */             {
/*  877 */               trig_type = "Prepaid Pooling Number";
/*      */             }
/*      */ 
/*  880 */             if (stype.equals("111"))
/*      */             {
/*  882 */               trig_type = "Postpaid Ready SIM";
/*      */             }
/*      */ 
/*      */           }
/*      */ 
/*  888 */           String timestamp = token[0] + " " + token[1].split(",")[0];
/*  889 */           long dateInLong = 0L;
/*      */           try
/*      */           {
/*  892 */             DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  893 */             Date date = formatter.parse(timestamp);
/*  894 */             dateInLong = date.getTime();
/*      */           }
/*      */           catch (Exception e)
/*      */           {
/*      */           }
/*      */ 
/*  901 */           if ((lasttime > 0L) && (dateInLong >= getNext(lasttime, interval)))
/*      */           {
/*  903 */             TLogger.info("Write output to file(" + output + "DDC_OPER_IN." + stime + ".dat)");
/*  904 */             TLogger.info("Write output to file(" + output + "DDC_OPER_PNP." + stime + ".dat)");
/*  905 */             for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */             {
/*  907 */               triggerInfo grp = (triggerInfo)iter.next();
/*      */               String outs;
/*      */               Date outtime;
/*      */               DateFormat formatter;
/*  908 */               if (grp.smaf)
/*      */               {
/*  910 */                 outs = "";
/*  911 */                 outs = outs + node + ",";
/*  912 */                 outs = outs + site + ",";
/*  913 */                 outs = outs + grp.trig_type + ",";
/*      */ 
/*  915 */                 outtime = new Date(getPrevious(lasttime, interval));
/*      */                 try
/*      */                 {
/*  918 */                   formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  919 */                   outs = outs + formatter.format(outtime) + ",";
/*      */                 }
/*      */                 catch (Exception e)
/*      */                 {
/*      */                 }
/*      */ 
/*  926 */                 outs = outs + "S,";
/*  927 */                 outs = outs + ",";
/*  928 */                 outs = outs + String.valueOf(grp.tran_count) + ",";
/*  929 */                 outs = outs + filepath.substring(sep + 1);
/*  930 */                 out.write(outs + "\n");
/*      */               }
/*      */               else
/*      */               {
/*  934 */                 outs = "";
/*  935 */                 outs = outs + node + ",";
/*  936 */                 outs = outs + site + ",";
/*  937 */                 outs = outs + grp.trig_type + ",";
/*      */ 
/*  939 */                 outtime = new Date(getPrevious(lasttime, interval));
/*      */                 try
/*      */                 {
							//ECT
/*  942 */                   SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  943 */                   outs = outs + e.format(outtime) + ",";
/*      */                 }
/*      */                 catch (Exception e)
/*      */                 {
/*      */                 }
/*      */ 
/*  950 */                 outs = outs + "S,";
/*  951 */                 outs = outs + ",";
/*  952 */                 outs = outs + String.valueOf(grp.tran_count) + ",";
/*  953 */                 outs = outs + filepath.substring(sep + 1);
/*  954 */                 out2.write(outs + "\n");
/*      */               }
/*      */ 
/*      */             }
/*      */ 
/*  960 */             src.clear();
/*      */           }
/*      */ 
/*  964 */           lasttime = dateInLong;
/*  965 */           if (!(trig_type.equals("xxx")))
/*      */           {
/*  967 */             triggerInfo grp = getGroup(trig_type, src, smaf, getPrevious(lasttime, interval));
/*      */ 
/*  969 */             grp.filename = filepath.substring(sep + 1);
/*  970 */             grp.filetime = dateFromFile.getTime();
/*  971 */             grp.tran_count += 1;
/*      */           }
/*      */ 
/*      */         }
/*      */ 
/*  978 */         if (src.size() > 0)
/*      */         {
/*  980 */           TLogger.info("Write output to file(" + output + "DDC_OPER_IN." + stime + ".dat)");
/*  981 */           TLogger.info("Write output to file(" + output + "DDC_OPER_PNP." + stime + ".dat)");
/*  982 */           for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */           {
/*  984 */             triggerInfo grp = (triggerInfo)iter.next();
/*      */             String outs;
/*      */             Date outtime;
/*      */             DateFormat formatter;
/*  985 */             if (grp.smaf)
/*      */             {
/*  987 */               outs = "";
/*  988 */               outs = outs + node + ",";
/*  989 */               outs = outs + site + ",";
/*  990 */               outs = outs + grp.trig_type + ",";
/*      */ 
/*  992 */               outtime = new Date(getPrevious(lasttime, interval));
/*      */               try
/*      */               {
/*  995 */                 formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/*  996 */                 outs = outs + formatter.format(outtime) + ",";
/*      */               }
/*      */               catch (Exception e)
/*      */               {
/*      */               }
/*      */ 
/* 1003 */               outs = outs + "S,";
/* 1004 */               outs = outs + ",";
/* 1005 */               outs = outs + String.valueOf(grp.tran_count) + ",";
/* 1006 */               outs = outs + filepath.substring(sep + 1);
/* 1007 */               out.write(outs + "\n");
/*      */             }
/*      */             else
/*      */             {
/* 1011 */               outs = "";
/* 1012 */               outs = outs + node + ",";
/* 1013 */               outs = outs + site + ",";
/* 1014 */               outs = outs + grp.trig_type + ",";
/*      */ 
/* 1016 */               outtime = new Date(getPrevious(lasttime, interval));
/*      */               try
/*      */               {
						// ECT
/* 1019 */                 SimpleDateFormat e = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/* 1020 */                 outs = outs + e.format(outtime) + ",";
/*      */               }
/*      */               catch (Exception e)
/*      */               {
/*      */               }
/*      */ 
/* 1027 */               outs = outs + "S,";
/* 1028 */               outs = outs + ",";
/* 1029 */               outs = outs + String.valueOf(grp.tran_count) + ",";
/* 1030 */               outs = outs + filepath.substring(sep + 1);
/* 1031 */               out2.write(outs + "\n");
/*      */             }
/*      */ 
/*      */           }
/*      */ 
/* 1037 */           src.clear();
/*      */         }
/*      */ 
/* 1042 */         out.close();
/* 1043 */         out2.close();
/* 1044 */         in.close();
/*      */ 
/* 1049 */         TLogger.info("complete process DDC DES counter file");
/*      */ 
/* 1052 */         return true;
/*      */       }
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1059 */       TLogger.error("Error: " + e.getMessage());
/* 1060 */       return false;
/*      */     }
/*      */ 
/* 1063 */     return false;
/*      */   }
/*      */ 
/*      */   public static boolean processCounterSMAF(String path, String output, Date dateFromFile, String node, String site, int interval)
/*      */   {
/* 1068 */     mode = 4;
/* 1069 */     if (!(output.endsWith(File.separator))) {
/* 1070 */       output = output + File.separator;
/*      */     }
/*      */ 
/*      */     try
/*      */     {
/* 1082 */       String filepath = path;
/* 1083 */       if (new File(filepath).exists())
/*      */       {
/* 1086 */         int sep = filepath.lastIndexOf(File.separator);
/* 1087 */         int dot = filepath.lastIndexOf(".");
/* 1088 */         TLogger.info("start process DDC SMAF counter file (" + filepath + ")");
/*      */ 
/* 1094 */         String stime = "";
/*      */         try
/*      */         {
/* 1097 */           DateFormat formatter = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/* 1098 */           stime = formatter.format(dateFromFile);
/*      */         }
/*      */         catch (Exception e)
/*      */         {
/*      */         }
/*      */ 
/* 1106 */         FileInputStream fstream = new FileInputStream(filepath);
/*      */ 
/* 1108 */         DataInputStream in = new DataInputStream(fstream);
/* 1109 */         BufferedReader br = new BufferedReader(new InputStreamReader(in));
/* 1110 */         FileWriter fostream = new FileWriter(output + "DDC_OPER_IN." + stime + ".dat", true);
/* 1111 */         TLogger.info("Write to file " + output + "DDC_OPER_PNP." + stime + ".dat");
/* 1112 */         BufferedWriter out = new BufferedWriter(fostream);
/*      */ 
/* 1118 */         src.clear();
/* 1119 */         String[] cm = { "{", "}", "[", "]" };

// ECT fix
/* 1120 */        // String[] con3 = { "74", "75", "76", "77", "78", "79", "80", "81", "82" };
				String[] con3 = { "49","50","51","52","53","54","55","56","57","74", "75", "76", "77", "78", "79", "80", "81", "82" };
/* 1121 */         long lasttime = 0L;
				  // ECT
/*      */    String strLine;
/* 1123 */         while ((strLine = br.readLine()) != null)
/*      */         {
/*      */        
/* 1126 */           String[] token = getSplit(strLine, " ", cm);
/* 1127 */           String trig_type = "xxx";
/*      */ 
/* 1129 */           String stype = "";
/* 1130 */           int st = strLine.indexOf("STYPE:");
/*      */ 
/* 1132 */           if (st > -1)
/*      */           {
/* 1134 */             int sp = strLine.indexOf(" ", st + 7);
/* 1135 */             stype = strLine.substring(st + 7, sp);
/*      */           }
/*      */ 
/* 1138 */           if (!(stype.equals("")))
/*      */           {
/* 1140 */             for (int k = 0; k < con3.length; ++k)
/*      */             {
/* 1142 */               if (!(con3[k].equals(stype)))
/*      */                 continue;
/* 1144 */               trig_type = "Prepaid Fix Number";
/* 1145 */               break;
/*      */             }
/*      */ 
/*      */           }
/*      */ 
/* 1150 */           if (stype.equals("6"))
/*      */           {
/* 1152 */             trig_type = "Prepaid Pooling Number";
/*      */           }
/*      */ 
/* 1155 */           if (stype.equals("111"))
/*      */           {
/* 1157 */             trig_type = "Postpaid Ready SIM";
/*      */           }
/*      */ 
/* 1161 */           String timestamp = token[0] + " " + token[1].split(",")[0];
/* 1162 */           long dateInLong = 0L;
/*      */           try
/*      */           {
/* 1165 */             DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/* 1166 */             Date date = formatter.parse(timestamp);
/* 1167 */             dateInLong = date.getTime();
/*      */           }
/*      */           catch (Exception e)
/*      */           {
/*      */           }
/*      */           triggerInfo2 grp;
/* 1174 */           if ((lasttime > 0L) && (dateInLong >= getNext(lasttime, interval)))
/*      */           {
/* 1176 */             TLogger.info("Write output to file(" + output + "DDC_OPER_IN." + stime + ".dat)");
/* 1177 */             for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */             {
/* 1179 */               grp = (triggerInfo2)iter.next();
/*      */ 
/* 1182 */               String outs = "";
/* 1183 */               outs = outs + node + ",";
/* 1184 */               outs = outs + site + ",";
/* 1185 */               outs = outs + grp.trig_type + ",";
/*      */ 
/* 1187 */               Date outtime = new Date(getPrevious(lasttime, interval));
/*      */               try
/*      */               {
/* 1190 */                 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/* 1191 */                 outs = outs + formatter.format(outtime) + ",";
/*      */               }
/*      */               catch (Exception e)
/*      */               {
/*      */               }
/*      */ 
/* 1198 */               outs = outs + "F,";
/* 1199 */               outs = outs + grp.error_code + ",";
/* 1200 */               outs = outs + String.valueOf(grp.tran_count) + ",";
/* 1201 */               outs = outs + filepath.substring(sep + 1);
/* 1202 */               out.write(outs + "\n");
/*      */             }
/*      */ 
/* 1209 */             src.clear();
/*      */           }
/*      */ 
/* 1212 */           lasttime = dateInLong;
/* 1213 */           String errorcode = "";
/* 1214 */           for (int i = 3; i < token.length; ++i)
/*      */           {
/* 1216 */             if (((!(token[i].startsWith("errorCode"))) && (!(token[i].startsWith("SMAF-ErrorCode")))) || (i + 1 >= token.length))
/*      */               continue;
/* 1218 */             errorcode = token[(i + 1)];
/*      */           }
/*      */ 
/* 1223 */           if (!(trig_type.equals("xxx")))
/*      */           {
/* 1225 */             grp = getGroup2(trig_type, errorcode, src, getNext(lasttime, interval));
/* 1226 */             grp.filename = filepath.substring(sep + 1);
/* 1227 */             grp.filetime = dateFromFile.getTime();
/* 1228 */             grp.tran_count += 1;
/*      */           }
/*      */ 
/*      */         }
/*      */ 
/* 1233 */         if (src.size() > 0)
/*      */         {
/* 1235 */           TLogger.info("Write output to file(" + output + "DDC_OPER_IN." + stime + ".dat)");
/* 1236 */           for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */           {
/* 1238 */             triggerInfo2 grp = (triggerInfo2)iter.next();
/*      */ 
/* 1241 */             String outs = "";
/* 1242 */             outs = outs + node + ",";
/* 1243 */             outs = outs + site + ",";
/* 1244 */             outs = outs + grp.trig_type + ",";
/*      */ 
/* 1246 */             Date outtime = new Date(getPrevious(lasttime, interval));
/*      */             try
/*      */             {
/* 1249 */               DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/* 1250 */               outs = outs + formatter.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
/*      */             }
/*      */ 
/* 1257 */             outs = outs + "F,";
/* 1258 */             outs = outs + grp.error_code + ",";
/* 1259 */             outs = outs + String.valueOf(grp.tran_count) + ",";
/* 1260 */             outs = outs + filepath.substring(sep + 1);
/* 1261 */             out.write(outs + "\n");
/*      */           }
/*      */ 
/* 1268 */           src.clear();
/*      */         }
/*      */ 
/* 1274 */         out.close();
/* 1275 */         in.close();
/*      */ 
/* 1278 */         TLogger.info("complete process DDC SMAF counter file");
/*      */ 
/* 1280 */         return true;
/*      */       }
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1287 */       TLogger.error("Error: " + e.getMessage());
/* 1288 */       return false;
/*      */     }
/*      */ 
/* 1291 */     return false;
/*      */   }
/*      */ 
/*      */   public static boolean processCounterPNP(String path, String output, Date dateFromFile, String node, String site, int interval)
/*      */   {
/* 1296 */     mode = 3;
/* 1297 */     if (!(output.endsWith(File.separator))) {
/* 1298 */       output = output + File.separator;
/*      */     }
/*      */ 
/*      */     try
/*      */     {
/* 1310 */       String filepath = path;
/* 1311 */       if (new File(filepath).exists())
/*      */       {
/* 1314 */         int sep = filepath.lastIndexOf(File.separator);
/* 1315 */         int dot = filepath.lastIndexOf(".");
/* 1316 */         TLogger.info("start process DDC PNP counter file (" + filepath + ")");
/*      */ 
/* 1322 */         String stime = "";
/*      */         try
/*      */         {
/* 1325 */           DateFormat formatter = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/* 1326 */           stime = formatter.format(dateFromFile);
/*      */         }
/*      */         catch (Exception e)
/*      */         {
/*      */         }
/*      */ 
/* 1334 */         FileInputStream fstream = new FileInputStream(filepath);
/*      */ 
/* 1336 */         DataInputStream in = new DataInputStream(fstream);
/* 1337 */         BufferedReader br = new BufferedReader(new InputStreamReader(in));
/* 1338 */         FileWriter fostream = new FileWriter(output + "DDC_OPER_PNP." + stime + ".dat", true);
/* 1339 */         BufferedWriter out = new BufferedWriter(fostream);
/*      */ 
/* 1345 */         src.clear();
/* 1346 */         String[] cm = { "{", "}", "[", "]" };
//ECT fix
///* 1347 */         String[] con3 = { "74", "75", "76", "77", "78", "79", "80", "81", "82" };

					String[] con3 = { "49","50","51","52","53","54","55","56","57","74", "75", "76", "77", "78", "79", "80", "81", "82" };
/*      */ 
/* 1349 */         long lasttime = 0L;
/*      */ 

					//ECT
					String strLine;
/* 1351 */         while ((strLine = br.readLine()) != null)
/*      */         {
/*      */          
/* 1354 */           String[] token = getSplit(strLine, " ", cm);
/* 1355 */           String trig_type = "xxx";
/* 1356 */           String stype = "";
/* 1357 */           int st = strLine.indexOf("STYPE:");
/*      */ 
/* 1359 */           if (st > -1)
/*      */           {
/* 1361 */             int sp = strLine.indexOf(" ", st + 7);
/* 1362 */             stype = strLine.substring(st + 7, sp);
/*      */           }
/*      */ 
/* 1365 */           if (!(stype.equals("")))
/*      */           {
/* 1367 */             for (int k = 0; k < con3.length; ++k)
/*      */             {
/* 1369 */               if (!(con3[k].equals(stype)))
/*      */                 continue;
/* 1371 */               trig_type = "Prepaid Fix Number";
/* 1372 */               break;
/*      */             }
/*      */ 
/*      */           }
/*      */ 
/* 1377 */           if (stype.equals("6"))
/*      */           {
/* 1379 */             trig_type = "Prepaid Pooling Number";
/*      */           }
/*      */ 
/* 1382 */           if (stype.equals("111"))
/*      */           {
/* 1384 */             trig_type = "Postpaid Ready SIM";
/*      */           }
/*      */ 
/* 1387 */           String timestamp = token[0] + " " + token[1].split(",")[0];
/* 1388 */           long dateInLong = 0L;
/*      */           try
/*      */           {
/* 1391 */             DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/* 1392 */             Date date = formatter.parse(timestamp);
/* 1393 */             dateInLong = date.getTime();
/*      */           }
/*      */           catch (Exception e)
/*      */           {
/*      */           }
/*      */           triggerInfo2 grp;
/* 1400 */           if ((lasttime > 0L) && (dateInLong >= getNext(lasttime, interval)))
/*      */           {
/* 1402 */             TLogger.info("Write output to file(" + output + "DDC_OPER_PNP." + stime + ".dat)");
/* 1403 */             for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */             {
/* 1405 */               grp = (triggerInfo2)iter.next();
/*      */ 
/* 1408 */               String outs = "";
/* 1409 */               outs = outs + node + ",";
/* 1410 */               outs = outs + site + ",";
/* 1411 */               outs = outs + grp.trig_type + ",";
/*      */ 
/* 1413 */               Date outtime = new Date(getPrevious(lasttime, interval));
/*      */               try
/*      */               {
/* 1416 */                 DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/* 1417 */                 outs = outs + formatter.format(outtime) + ",";
/*      */               }
/*      */               catch (Exception e)
/*      */               {
/*      */               }
/*      */ 
/* 1424 */               outs = outs + "F,";
/* 1425 */               outs = outs + grp.error_code + ",";
/* 1426 */               outs = outs + String.valueOf(grp.tran_count) + ",";
/* 1427 */               outs = outs + filepath.substring(sep + 1);
/* 1428 */               out.write(outs + "\n");
/*      */             }
/*      */ 
/* 1435 */             src.clear();
/*      */           }
/*      */ 
/* 1438 */           lasttime = dateInLong;
/* 1439 */           String errorcode = "";
/* 1440 */           for (int i = 3; i < token.length; ++i)
/*      */           {
/* 1442 */             if ((!(token[i].startsWith("responseCode"))) || (i + 1 >= token.length))
/*      */               continue;
/* 1444 */             errorcode = token[(i + 1)];
/*      */           }
/*      */ 
/* 1449 */           if (!(trig_type.equals("xxx")))
/*      */           {
/* 1451 */             grp = getGroup2(trig_type, errorcode, src, getPrevious(lasttime, interval));
/* 1452 */             grp.filename = filepath.substring(sep + 1);
/* 1453 */             grp.filetime = dateFromFile.getTime();
/*      */ 
/* 1455 */             grp.tran_count += 1;
/*      */           }
/*      */ 
/*      */         }
/*      */ 
/* 1461 */         if (src.size() > 0)
/*      */         {
/* 1463 */           TLogger.info("Write output to file(" + output + "DDC_OPER_PNP." + stime + ".dat)");
/* 1464 */           for (Iterator iter = src.iterator(); iter.hasNext(); )
/*      */           {
/* 1466 */             triggerInfo2 grp = (triggerInfo2)iter.next();
/*      */ 
/* 1469 */             String outs = "";
/* 1470 */             outs = outs + node + ",";
/* 1471 */             outs = outs + site + ",";
/* 1472 */             outs = outs + grp.trig_type + ",";
/*      */ 
/* 1474 */             Date outtime = new Date(getPrevious(lasttime, interval));
/*      */             try
/*      */             {
/* 1477 */               DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US);
/* 1478 */               outs = outs + formatter.format(outtime) + ",";
/*      */             }
/*      */             catch (Exception e)
/*      */             {
/*      */             }
/*      */ 
/* 1485 */             outs = outs + "F,";
/* 1486 */             outs = outs + grp.error_code + ",";
/* 1487 */             outs = outs + String.valueOf(grp.tran_count) + ",";
/* 1488 */             outs = outs + filepath.substring(sep + 1);
/* 1489 */             out.write(outs + "\n");
/*      */           }
/*      */ 
/* 1496 */           src.clear();
/*      */         }
/*      */ 
/* 1501 */         out.close();
/* 1502 */         in.close();
/*      */ 
/* 1505 */         TLogger.info("complete process DDC PNP counter file");
/*      */ 
/* 1507 */         return true;
/*      */       }
/*      */ 
/*      */     }
/*      */     catch (Exception e)
/*      */     {
/* 1514 */       TLogger.error("Error: " + e.getMessage());
/* 1515 */       return false;
/*      */     }
/*      */ 
/* 1518 */     return false;
/*      */   }
/*      */ 
/*      */   public static boolean processCounterMYSQL(String path, String output, Date dateFromFile)
/*      */   {
/* 1523 */     mode = 2;
/* 1524 */     if (!(output.endsWith(File.separator)))
/* 1525 */       output = output + File.separator;
/*      */     try {
/* 1527 */       String filepath = path;
/* 1528 */       if (new File(filepath).exists())
/*      */       {
/* 1530 */         int sep = filepath.lastIndexOf(File.separator);
/* 1531 */         int dot = filepath.lastIndexOf(".");
/* 1532 */         TLogger.info("start process DDC MYSQL counter file (" + filepath + ")");
/* 1533 */         FileInputStream fstream = new FileInputStream(filepath);
/* 1534 */         String stime = "";
/*      */         try
/*      */         {
/* 1537 */           DateFormat formatter = new SimpleDateFormat("yyyyMMddHH", Locale.US);
/* 1538 */           stime = formatter.format(dateFromFile);
/*      */         }
/*      */         catch (Exception e)
/*      */         {
/*      */         }
/*      */ 
/* 1546 */         DataInputStream in = new DataInputStream(fstream);
/* 1547 */         BufferedReader br = new BufferedReader(new InputStreamReader(in));
/* 1548 */         FileWriter fostream = new FileWriter(output + "DDC_OPER_IN." + stime + ".dat", true);
/* 1549 */         BufferedWriter out = new BufferedWriter(fostream);
/*      */ 
					// ECT
					String strLine;
/* 1551 */         while ((strLine = br.readLine()) != null)
/*      */         {
/*      */           
/* 1552 */           String outs = "";
/* 1553 */           String[] st = strLine.split(",");
/* 1554 */           outs = st[1] + "," + st[0] + ",," + st[3] + ",LC,0," + st[2] + "," + filepath.substring(sep + 1);
/* 1555 */           out.write(outs + "\n");
/*      */         }
/*      */ 
/* 1558 */         out.close();
/* 1559 */         in.close();
/*      */ 
/* 1563 */         TLogger.info("complete process DDC MYSQL counter file");
/*      */ 
/* 1568 */         return true;
/*      */       }
/*      */     }
/*      */     catch (Exception e) {
/* 1572 */       TLogger.error("Error: " + e.getMessage());
/* 1573 */       return false;
/*      */     }
/* 1575 */     return false;
/*      */   }
/*      */ 
/*      */   public static void main(String[] args)
/*      */   {
/* 1582 */     TLogger.getInstance("PNP", "d:\\temp\\des");
/*      */   }
/*      */ }

/* Location:           C:\Users\smileyopp\Desktop\DTAC\DthCounter.jar
 * Qualified Name:     th.co.esln.format.ddc.DDCprocess
 * JD-Core Version:    0.5.3
 */